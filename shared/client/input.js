parts.input = function(instances){
    var output = function(){
        instances.window.document.addEventListener('keydown', function(e){
            switch(e.keyCode) {
                case 87: output.leftStick[1] = 1.0; break
                case 83: output.leftStick[1] = -1.0; break
                case 65: output.leftStick[0] = -1.0; break
                case 68: output.leftStick[0] = 1.0; break
                case 73: output.rightStick[1] = 1.0; break
                case 75: output.rightStick[1] = -1.0; break
                case 74: output.rightStick[0] = -1.0; break
                case 76: output.rightStick[0] = 1.0; break                
            }
        })        
        
        instances.window.document.addEventListener('keyup', function(e){
            switch(e.keyCode) {
                case 87: output.leftStick[1] = 0.0; break
                case 83: output.leftStick[1] = 0.0; break
                case 65: output.leftStick[0] = 0.0; break
                case 68: output.leftStick[0] = 0.0; break
                case 73: output.rightStick[1] = 0.0; break
                case 75: output.rightStick[1] = 0.0; break
                case 74: output.rightStick[0] = 0.0; break
                case 76: output.rightStick[0] = 0.0; break                
            }            
        })
    }
    output.leftStick = [0, 0]
    output.rightStick = [0, 0]
    return output
}