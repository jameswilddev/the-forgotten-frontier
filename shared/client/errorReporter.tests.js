describe('errorReporter', function(){
    var instances, run
    
    beforeEach(function(){
        run = parts.errorReporter(instances = {
            window: {
                alert: jasmine.createSpy()
            },
            localizations: {
                errorPreamble: 'Test Preamble'
            }
        })  
    })
    
    it('calls alert to show the user the error', function(){
        run('Test Error') 
        expect(instances.window.alert).toHaveBeenCalledWith('Test PreambleTest Error')
        expect(instances.window.alert.calls.count()).toEqual(1)
    })
    
    it('logs the error through analytics when available', function(){
        instances.analytics = {
            error: jasmine.createSpy()
        }
        run('Test Error')  
        expect(instances.analytics.error).toHaveBeenCalledWith('Test Error')
        expect(instances.analytics.error.calls.count()).toEqual(1)
    })
})