/* On calling, draws a NDC-filling plane with position written in 2D to the 
   given vertex attribute id. */
parts.passGeometry = function(instances) {
   var buffer
   return function(position) {
      var gl = instances.canvas.context
      if(!buffer) {
         buffer = gl.createBuffer(gl.ARRAY_BUFFER)
         gl.bindBuffer(gl.ARRAY_BUFFER, buffer)
         gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
        	    -1.0, -1.0, 
        	    1.0, -1.0, 
        	    -1.0, 1.0, 
        	    1.0, 1.0
         ]), gl.STATIC_DRAW)
      } else gl.bindBuffer(gl.ARRAY_BUFFER, buffer)
      gl.enableVertexAttribArray(position)
      gl.vertexAttribPointer(position, 2, gl.FLOAT, false, 0, 0)
      gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4)
      gl.disableVertexAttribArray(position)      
   }
}