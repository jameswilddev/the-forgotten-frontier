parts.request = function(instances){
    var part = function(url, method, success, body) {
        var request = new part.XMLHttpRequest()
    	request.onreadystatechange = function(){
    		if(request.readyState != 4) return
    		if(request.status == 200) {
    		    if(success)
    			    success(request.responseText)
    		} else 
    		    instances.errorReporter('Failed to "' + method + '" "' + url + '".  (status code ' + request.status + ')')
    	}
    	request.open(method, url, true)
    	if(body) request.setRequestHeader('content-type', 'application/json')
    	request.send(JSON.stringify(body))
    }
    part.XMLHttpRequest = XMLHttpRequest
    return part
}