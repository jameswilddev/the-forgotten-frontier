describe('eventLoop', function(){
    var part, instances
    beforeEach(function(){
        part = parts.eventLoop(instances = {})
    })
    describe('on calling', function(){
        var event, timer
        beforeEach(function(){
            instances.canvas = {
                context: {
                    
                },
                canvas: {
                    
                }
            }
            instances.configuration = {
                ticksPerSecond: 30,
                maximumDelta: 0.55
            }
            instances.timer = jasmine.createSpy()
            timer = {
                subscribe: 'Test Tick Subscribe'
            }
            instances.timer.and.callFake(function(interval) {
                if(interval == 30) return timer
            })
            instances.event = jasmine.createSpy()
            instances.event.and.returnValue(event = {
                subscribe: 'Test Render Subscribe'
            })
            instances.window = {
                requestAnimationFrame: jasmine.createSpy()            
            }
            part()
        })
        
        it('creates a timer and stores its subscribe function', function(){
            expect(instances.timer).toHaveBeenCalledWith(30)
            expect(instances.timer.calls.count()).toEqual(1)
            
            expect(part.tick).toEqual('Test Tick Subscribe')
        })
        
        it('creates an event and stores its subscribe function', function(){
            expect(instances.event)
            expect(instances.event.calls.count()).toEqual(1)
            
            expect(part.render).toEqual('Test Render Subscribe')
        })
        
        it('sets a render callback for the next frame', function(){
            expect(instances.window.requestAnimationFrame.calls.count()).toEqual(1)
        })
        
        describe('render callback', function(){
            beforeEach(function(){
                instances.canvas.context.viewport = jasmine.createSpy()
                timer.advance = jasmine.createSpy()
                event.raise = jasmine.createSpy()
            })
            
            it('updates and stores the canvas size', function(){
                instances.window.innerWidth = 640
                instances.window.innerHeight = 480                
            
                event.raise.and.callFake(function(){
                    expect(part.width).toEqual(640)
                    expect(part.height).toEqual(480)                            
                    expect(instances.canvas.canvas.width).toEqual(640)
                    expect(instances.canvas.canvas.height).toEqual(480)
                    expect(instances.canvas.context.viewport).toHaveBeenCalledWith(0, 0, 640, 480)
                    expect(instances.canvas.context.viewport.calls.count()).toEqual(1)                        
                })
                
                instances.window.requestAnimationFrame.calls.argsFor(0)[0]()
            })

            it('advances the timer', function(){
                instances.window.requestAnimationFrame.calls.argsFor(0)[0](300)
                
                expect(timer.advance.calls.count()).toEqual(1)
                expect(timer.advance).toHaveBeenCalledWith(0.3)
            })
            
            it('respects the maximum delta', function(){
                instances.window.requestAnimationFrame.calls.argsFor(0)[0](600)
                
                expect(timer.advance.calls.count()).toEqual(1)
                expect(timer.advance).toHaveBeenCalledWith(0.55)
            })            
            
            it('has updated the framerate by the time the timer is advanced', function(){
                timer.advance.and.callFake(function(){
                    expect(part.framerate).toBeCloseTo(3)
                })
                
                instances.window.requestAnimationFrame.calls.argsFor(0)[0](300)
            })
            
            it('then copies the timer\'s progress', function(){
                timer.advance.and.callFake(function(){
                    timer.progress = 'Test Progress'
                })
                
                event.raise.and.callFake(function(){
                    expect(part.progress).toEqual('Test Progress')
                })
                
                instances.window.requestAnimationFrame.calls.argsFor(0)[0]()
            })
            
            it('then raises the event', function(){
                event.raise.and.callFake(function(){
                    expect(timer.advance.calls.count()).toEqual(1)
                })
                
                instances.window.requestAnimationFrame.calls.argsFor(0)[0]()
                
                expect(event.raise.calls.count()).toEqual(1)
            })
            
            it('sets itself up for the next frame', function(){
                instances.window.requestAnimationFrame.and.callFake(function(callback){
                    expect(timer.advance).toHaveBeenCalled()
                })
                
                instances.window.requestAnimationFrame.calls.argsFor(0)[0]()
                
                expect(instances.window.requestAnimationFrame.calls.count()).toEqual(2)
                expect(instances.window.requestAnimationFrame.calls.argsFor(0)[0]).toBe(instances.window.requestAnimationFrame.calls.argsFor(1)[0])
            })
            
            describe('on the next call', function(){
                it('advances by the difference', function(){
                    instances.window.requestAnimationFrame.calls.argsFor(0)[0](300)
                    instances.window.requestAnimationFrame.calls.argsFor(0)[0](500)
                    
                    expect(timer.advance).toHaveBeenCalledWith(0.2)
                })
                
                it('respects the maximum delta', function(){
                    instances.window.requestAnimationFrame.calls.argsFor(0)[0](300)
                    instances.window.requestAnimationFrame.calls.argsFor(0)[0](900)
                    
                    expect(timer.advance).toHaveBeenCalledWith(0.55)
                })                
                
                it('has updated the framerate by the time the timer is advanced', function(){
                    instances.window.requestAnimationFrame.calls.argsFor(0)[0](200)
                    
                    timer.advance.and.callFake(function(){
                        expect(part.framerate).toBeCloseTo(10)
                    })
                    
                    instances.window.requestAnimationFrame.calls.argsFor(0)[0](300)
                })                          
                
                it('stores the total time', function(){
                    instances.window.requestAnimationFrame.calls.argsFor(0)[0](300)
                    timer.advance.and.callFake(function(){
                        expect(part.time).toEqual(0.5)
                    })
                    instances.window.requestAnimationFrame.calls.argsFor(0)[0](500)
                    expect(timer.advance.calls.count()).toEqual(2)
                })
            })
        })         
    })
})