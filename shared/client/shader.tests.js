describe('shader', function(){
    var part, instances
    beforeEach(function(){
        part = parts.shader(instances = {
            canvas: {
                context: {
                    createShader: jasmine.createSpy(),
                    shaderSource: jasmine.createSpy(),
                    compileShader: jasmine.createSpy(),
                    getShaderParameter: jasmine.createSpy(),
                    COMPILE_STATUS: 'Test Compile Status'
                }
            }
        })
        instances.canvas.context.createShader.and.returnValue('Test Shader Id')
        instances.canvas.context.getShaderParameter.and.returnValue(true)
    })        
    
    it('creates a new shader', function(){
        part('Test Shader Type')
        expect(instances.canvas.context.createShader.calls.count()).toEqual(1)
        expect(instances.canvas.context.createShader).toHaveBeenCalledWith('Test Shader Type')
    })

    it('gives the source to the shader', function(){
        part(undefined, 'Test Source')
        expect(instances.canvas.context.shaderSource.calls.count()).toEqual(1)
        expect(instances.canvas.context.shaderSource).toHaveBeenCalledWith('Test Shader Id', 'Test Source')
    })

    it('compiles the shader', function(){
        instances.canvas.context.compileShader.and.callFake(function(){
            expect(instances.canvas.context.shaderSource).toHaveBeenCalled()
        })
        part()
        expect(instances.canvas.context.compileShader.calls.count()).toEqual(1)
        expect(instances.canvas.context.compileShader).toHaveBeenCalledWith('Test Shader Id')
    })

    it('checks whether the shader compiled successfully', function(){
        instances.canvas.context.getShaderParameter.and.callFake(function(){
            expect(instances.canvas.context.compileShader).toHaveBeenCalled()
            return true
        })
        part(undefined, undefined, 'Test Shader Filename')
        expect(instances.canvas.context.getShaderParameter.calls.count()).toEqual(1)
        expect(instances.canvas.context.getShaderParameter).toHaveBeenCalledWith('Test Shader Id', 'Test Compile Status')
    })

    describe('compilation successful', function(){
        it('returns the shader', function(){
            expect(part()).toEqual('Test Shader Id')
        })
    })

    describe('compilation failed', function(){
        beforeEach(function(){
            instances.canvas.context.getShaderParameter.and.returnValue(false)
            instances.errorReporter = jasmine.createSpy()
            instances.canvas.context.deleteShader = jasmine.createSpy()
            instances.canvas.context.getShaderInfoLog = jasmine.createSpy()
        })

        it('gets and reports the error', function(){
            instances.canvas.context.getShaderInfoLog.and.returnValue('Test Error')
            part(undefined, undefined, "Test Shader Filename")

            expect(instances.canvas.context.getShaderInfoLog.calls.count()).toEqual(1)
            expect(instances.canvas.context.getShaderInfoLog).toHaveBeenCalledWith('Test Shader Id')
            expect(instances.errorReporter.calls.count()).toEqual(1)
            expect(instances.errorReporter).toHaveBeenCalledWith('Error compiling shader "Test Shader Filename": "Test Error"')
        })

        it('deletes the shader', function(){
            instances.canvas.context.deleteShader.and.callFake(function(){
                expect(instances.canvas.context.getShaderInfoLog).toHaveBeenCalled()
            })
            
            part()
            
            expect(instances.canvas.context.deleteShader.calls.count()).toEqual(1)
            expect(instances.canvas.context.deleteShader).toHaveBeenCalledWith('Test Shader Id')
        })

        it('returns null', function(){
            expect(part()).toBeFalsy()
        })
    })
})