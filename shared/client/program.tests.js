describe('program', function(){
    var part, instances
    beforeEach(function(){
        part = parts.program(instances = {})
    })
    describe('on calling', function(){
        beforeEach(function(){
            instances.canvas = {
                context: {
                    VERTEX_SHADER: 'Test Vertex Shader',
                    FRAGMENT_SHADER: 'Test Fragment Shader'
                }
            }
        })
        
        describe('when the vertex shader fails to compile', function(){
            beforeEach(function(){
                instances.shader = jasmine.createSpy()
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
            })
            
            it('does not compile the fragment shader', function(){
                expect(instances.shader.calls.count()).toEqual(1)
                expect(instances.shader).toHaveBeenCalledWith('Test Vertex Shader', 'Test Vertex Source', 'Test Filename (vertex shader)')
            })
        })
        
        describe('when the vertex shader compiles but the fragment shader does not', function(){
            beforeEach(function(){
                instances.shader = jasmine.createSpy()
                instances.canvas.context.deleteShader = jasmine.createSpy()
                
                instances.shader.and.callFake(function(type, source, filename){
                    if(type != 'Test Vertex Shader') return
                    if(source != 'Test Vertex Source') return
                    if(filename != 'Test Filename (vertex shader)') return
                    return 'Test Vertex Id'
                })
                
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
            })
            
            it('compiles the vertex shader', function(){
                expect(instances.shader.calls.count()).toEqual(2)
                expect(instances.shader).toHaveBeenCalledWith('Test Vertex Shader', 'Test Vertex Source', 'Test Filename (vertex shader)')
            })
            
            it('attempts to compile the fragment shader', function(){
                expect(instances.shader.calls.count()).toEqual(2)
                expect(instances.shader).toHaveBeenCalledWith('Test Fragment Shader', 'Test Fragment Source', 'Test Filename (fragment shader)')
            }) 
            
            it('deletes the failed vertex shader', function(){
                expect(instances.canvas.context.deleteShader.calls.count()).toEqual(1)
                expect(instances.canvas.context.deleteShader).toHaveBeenCalledWith('Test Vertex Id')
            })
        })   
        
        describe('when both the vertex and fragment shaders compile but linking fails', function(){
            beforeEach(function(){
                instances.shader = jasmine.createSpy()
                instances.canvas.context.deleteShader = jasmine.createSpy()
                instances.canvas.context.createProgram = jasmine.createSpy()
                instances.canvas.context.createProgram.and.returnValue('Test Program Id')
                instances.canvas.context.attachShader = jasmine.createSpy()
                instances.canvas.context.detachShader = jasmine.createSpy()
                instances.canvas.context.linkProgram = jasmine.createSpy()
                instances.canvas.context.getProgramParameter = jasmine.createSpy()
                instances.canvas.context.getProgramInfoLog = jasmine.createSpy()
                instances.canvas.context.deleteProgram = jasmine.createSpy()
                instances.canvas.context.LINK_STATUS = 'Test Link Status'
                instances.errorReporter = jasmine.createSpy()
                
                instances.shader.and.callFake(function(type, source, filename){
                    if(type == 'Test Vertex Shader' 
                        && source == 'Test Vertex Source'
                        && filename == 'Test Filename (vertex shader)')
                        return 'Test Vertex Id'
                        
                    if(type == 'Test Fragment Shader' 
                        && source == 'Test Fragment Source'
                        && filename == 'Test Filename (fragment shader)')
                        return 'Test Fragment Id'                        
                })
            })
            
            it('compiles the vertex shader', function(){
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.shader.calls.count()).toEqual(2)
                expect(instances.shader).toHaveBeenCalledWith('Test Vertex Shader', 'Test Vertex Source', 'Test Filename (vertex shader)')                
            })
            
            it('compiles the fragment shader', function(){
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.shader.calls.count()).toEqual(2)
                expect(instances.shader).toHaveBeenCalledWith('Test Fragment Shader', 'Test Fragment Source', 'Test Filename (fragment shader)')                
            })
            
            it('creates a program', function(){
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.createProgram.calls.count()).toEqual(1)
            })
            
            it('attaches the vertex shader to the program', function(){
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.attachShader.calls.count()).toEqual(2)
                expect(instances.canvas.context.attachShader).toHaveBeenCalledWith('Test Program Id', 'Test Vertex Id')                
            })
            
            it('attaches the fragment shader to the program', function(){
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.attachShader.calls.count()).toEqual(2)
                expect(instances.canvas.context.attachShader).toHaveBeenCalledWith('Test Program Id', 'Test Fragment Id')                                
            })
            
            it('then links the program', function(){
                instances.canvas.context.linkProgram.and.callFake(function(program){
                    if(program != 'Test Program Id') return
                    expect(instances.canvas.context.attachShader.calls.count()).toEqual(2)
                })
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.linkProgram.calls.count()).toEqual(1)
                expect(instances.canvas.context.linkProgram).toHaveBeenCalledWith('Test Program Id')                                
            })
            
            it('then detaches the vertex shader', function(){
                instances.canvas.context.detachShader.and.callFake(function(program, shader){
                    if(program != 'Test Program Id' || shader != 'Test Vertex Id') return
                    expect(instances.canvas.context.linkProgram).toHaveBeenCalled()
                })
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.detachShader.calls.count()).toEqual(2)
                expect(instances.canvas.context.detachShader).toHaveBeenCalledWith('Test Program Id', 'Test Vertex Id')                                                
            })
            
            it('then deletes the vertex shader', function(){
                instances.canvas.context.deleteShader.and.callFake(function(shader){
                    if(shader != 'Test Vertex Id') return
                    expect(instances.canvas.context.detachShader).toHaveBeenCalledWith('Test Program Id', 'Test Vertex Id')
                })
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.deleteShader.calls.count()).toEqual(2)
                expect(instances.canvas.context.deleteShader).toHaveBeenCalledWith('Test Vertex Id')                                                
            })                
            
            it('then detaches the fragment shader', function(){
                instances.canvas.context.detachShader.and.callFake(function(program, shader){
                    if(program != 'Test Program Id' || shader != 'Test Fragment Id') return
                    expect(instances.canvas.context.linkProgram).toHaveBeenCalled()
                })
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.detachShader.calls.count()).toEqual(2)
                expect(instances.canvas.context.detachShader).toHaveBeenCalledWith('Test Program Id', 'Test Fragment Id')                                                                                
            })
            
            it('then deletes the fragment shader', function(){
                instances.canvas.context.deleteShader.and.callFake(function(shader){
                    if(shader != 'Test Fragment Id') return
                    expect(instances.canvas.context.detachShader).toHaveBeenCalledWith('Test Program Id', 'Test Fragment Id')
                })
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.deleteShader.calls.count()).toEqual(2)
                expect(instances.canvas.context.deleteShader).toHaveBeenCalledWith('Test Fragment Id')                                                                
            })     
            
            it('then checks that the program linked', function(){
                instances.canvas.context.getProgramParameter.and.callFake(function(program, parameter){
                    if(program != 'Test Program Id' || parameter != 'Test Link Status') return
                    expect(instances.canvas.context.linkProgram).toHaveBeenCalled()
                })
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.getProgramParameter.calls.count()).toEqual(1)
                expect(instances.canvas.context.getProgramParameter).toHaveBeenCalledWith('Test Program Id', 'Test Link Status')                                                
            })
            
            it('reports the error', function(){
                instances.canvas.context.getProgramInfoLog.and.callFake(function(program){
                    if(program == 'Test Program Id') return 'Test Error'
                })
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.errorReporter.calls.count()).toEqual(1)
                expect(instances.errorReporter).toHaveBeenCalledWith('Failed to link shader for "Test Filename": "Test Error"')
            })
            
            it('returns null', function(){
                expect(part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')).toBeFalsy()
            })
        })
        
        describe('when both the vertex and fragment shaders compile and linking succeeds', function(){
            beforeEach(function(){
                instances.shader = jasmine.createSpy()
                instances.canvas.context.deleteShader = jasmine.createSpy()
                instances.canvas.context.createProgram = jasmine.createSpy()
                instances.canvas.context.createProgram.and.returnValue('Test Program Id')
                instances.canvas.context.attachShader = jasmine.createSpy()
                instances.canvas.context.detachShader = jasmine.createSpy()
                instances.canvas.context.linkProgram = jasmine.createSpy()
                instances.canvas.context.getProgramParameter = jasmine.createSpy()
                instances.canvas.context.deleteProgram = jasmine.createSpy()
                instances.canvas.context.LINK_STATUS = 'Test Link Status'
                
                instances.shader.and.callFake(function(type, source, filename){
                    if(type == 'Test Vertex Shader' 
                        && source == 'Test Vertex Source'
                        && filename == 'Test Filename (vertex shader)')
                        return 'Test Vertex Id'
                        
                    if(type == 'Test Fragment Shader' 
                        && source == 'Test Fragment Source'
                        && filename == 'Test Filename (fragment shader)')
                        return 'Test Fragment Id'                        
                })
                
                instances.canvas.context.getProgramParameter.and.callFake(function(program, parameter){
                    if(program == 'Test Program Id' && parameter == 'Test Link Status') return true
                })
            })            
            
            it('compiles the vertex shader', function(){
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.shader.calls.count()).toEqual(2)
                expect(instances.shader).toHaveBeenCalledWith('Test Vertex Shader', 'Test Vertex Source', 'Test Filename (vertex shader)')                                
            })
            
            it('compiles the fragment shader', function(){
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.shader.calls.count()).toEqual(2)
                expect(instances.shader).toHaveBeenCalledWith('Test Fragment Shader', 'Test Fragment Source', 'Test Filename (fragment shader)')                                
            })
            
            it('creates a program', function(){
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.createProgram.calls.count()).toEqual(1)                
            })
            
            it('attaches the vertex shader to the program', function(){
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.attachShader.calls.count()).toEqual(2)
                expect(instances.canvas.context.attachShader).toHaveBeenCalledWith('Test Program Id', 'Test Vertex Id')                                
            })
            
            it('attaches the fragment shader to the program', function(){
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.attachShader.calls.count()).toEqual(2)
                expect(instances.canvas.context.attachShader).toHaveBeenCalledWith('Test Program Id', 'Test Fragment Id')                                                
            })
            
            it('then links the program', function(){
                instances.canvas.context.linkProgram.and.callFake(function(program){
                    if(program != 'Test Program Id') return
                    expect(instances.canvas.context.attachShader.calls.count()).toEqual(2)
                })
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.linkProgram.calls.count()).toEqual(1)
                expect(instances.canvas.context.linkProgram).toHaveBeenCalledWith('Test Program Id')                                                
            })
            
            it('then detaches the vertex shader', function(){
                instances.canvas.context.detachShader.and.callFake(function(program, shader){
                    if(program != 'Test Program Id' || shader != 'Test Vertex Id') return
                    expect(instances.canvas.context.linkProgram).toHaveBeenCalled()
                })
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.detachShader.calls.count()).toEqual(2)
                expect(instances.canvas.context.detachShader).toHaveBeenCalledWith('Test Program Id', 'Test Vertex Id')                                                                
            })
            
            it('then deletes the vertex shader', function(){
                instances.canvas.context.deleteShader.and.callFake(function(shader){
                    if(shader != 'Test Vertex Id') return
                    expect(instances.canvas.context.detachShader).toHaveBeenCalledWith('Test Program Id', 'Test Vertex Id')
                })
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.deleteShader.calls.count()).toEqual(2)
                expect(instances.canvas.context.deleteShader).toHaveBeenCalledWith('Test Vertex Id')                                                                
            })                
            
            it('then detaches the fragment shader', function(){
                instances.canvas.context.detachShader.and.callFake(function(program, shader){
                    if(program != 'Test Program Id' || shader != 'Test Fragment Id') return
                    expect(instances.canvas.context.linkProgram).toHaveBeenCalled()
                })
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.detachShader.calls.count()).toEqual(2)
                expect(instances.canvas.context.detachShader).toHaveBeenCalledWith('Test Program Id', 'Test Fragment Id')                                                                                                
            })
            
            it('then deletes the fragment shader', function(){
                instances.canvas.context.deleteShader.and.callFake(function(shader){
                    if(shader != 'Test Fragment Id') return
                    expect(instances.canvas.context.detachShader).toHaveBeenCalledWith('Test Program Id', 'Test Fragment Id')
                })
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.deleteShader.calls.count()).toEqual(2)
                expect(instances.canvas.context.deleteShader).toHaveBeenCalledWith('Test Fragment Id')                                                                                
            })     
            
            it('then checks that the program linked', function(){
                instances.canvas.context.getProgramParameter.and.callFake(function(program, parameter){
                    if(program != 'Test Program Id' || parameter != 'Test Link Status') return
                    expect(instances.canvas.context.linkProgram).toHaveBeenCalled()
                    return true
                })
                part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')
                expect(instances.canvas.context.getProgramParameter.calls.count()).toEqual(1)
                expect(instances.canvas.context.getProgramParameter).toHaveBeenCalledWith('Test Program Id', 'Test Link Status')                                                
            })
            
            it('returns the program id', function(){
                expect(part('Test Vertex Source', 'Test Fragment Source', 'Test Filename')).toEqual('Test Program Id')
            })
        })            
    })
})