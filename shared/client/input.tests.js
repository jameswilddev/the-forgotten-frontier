describe('input', function(){
    var part, instances
    beforeEach(function(){
        part = parts.input(instances = {})
    })
    
    it('defines a leftStick vector', function(){
        expect(part.leftStick).toEqual([0, 0])
    })
    
    it('defines a rightStick vector', function(){
        expect(part.rightStick).toEqual([0, 0])
    })
    
    describe('on initialization', function(){
        beforeEach(function(){
            instances.window = {
                document: {
                    addEventListener: jasmine.createSpy()
                }
            }    
            part()
        })
        describe('keyboard', function(){
            beforeEach(function(){
                part.leftStick[0] = 0.3
                part.leftStick[1] = -0.4
                part.rightStick[0] = 0.7
                part.rightStick[1] = 0.8
            })
            
            it('binds to the document\'s keydown event', function(){
                expect(instances.window.document.addEventListener).toHaveBeenCalledWith('keydown', jasmine.any(Function))
            })
            
            it('binds to the document\'s keyup event', function(){
                expect(instances.window.document.addEventListener).toHaveBeenCalledWith('keyup', jasmine.any(Function))
            })
            
            describe('on pressing', function(){
                var handler
                beforeEach(function(){
                    for(var call = 0; call < instances.window.document.addEventListener.calls.count(); call++) {
                        if(instances.window.document.addEventListener.calls.argsFor(call)[0] == 'keydown')
                            handler = instances.window.document.addEventListener.calls.argsFor(call)[1]
                    }
                })
                
                it('w it pushes the left stick up', function(){
                    handler({keyCode: 87})
                    expect(part.leftStick).toEqual([0.3, 1.0])
                    expect(part.rightStick).toEqual([0.7, 0.8])
                })
                
                it('s it pushes the left stick down', function(){
                    handler({keyCode: 83})
                    expect(part.leftStick).toEqual([0.3, -1.0])
                    expect(part.rightStick).toEqual([0.7, 0.8])                    
                })          
                
                it('a pushes the left stick left', function(){
                    handler({keyCode: 65})
                    expect(part.leftStick).toEqual([-1.0, -0.4])
                    expect(part.rightStick).toEqual([0.7, 0.8])                    
                })            
                
                it('d pushes the left stick right', function(){
                    handler({keyCode: 68})
                    expect(part.leftStick).toEqual([1.0, -0.4])
                    expect(part.rightStick).toEqual([0.7, 0.8])                                        
                })     
                
                it('i it pushes the right stick up', function(){
                    handler({keyCode: 73})
                    expect(part.leftStick).toEqual([0.3, -0.4])
                    expect(part.rightStick).toEqual([0.7, 1.0])
                })
                
                it('k it pushes the right stick down', function(){
                    handler({keyCode: 75})
                    expect(part.leftStick).toEqual([0.3, -0.4])
                    expect(part.rightStick).toEqual([0.7, -1.0])                    
                })          
                
                it('j pushes the right stick left', function(){
                    handler({keyCode: 74})
                    expect(part.leftStick).toEqual([0.3, -0.4])
                    expect(part.rightStick).toEqual([-1.0, 0.8])                    
                })            
                
                it('l pushes the right stick right', function(){
                    handler({keyCode: 76})
                    expect(part.leftStick).toEqual([0.3, -0.4])
                    expect(part.rightStick).toEqual([1.0, 0.8])                                        
                })                   
                
                it('other keys do nothing', function(){
                    handler({keyCode: 45})
                    expect(part.leftStick).toEqual([0.3, -0.4])
                    expect(part.rightStick).toEqual([0.7, 0.8])  
                })            
            })
            
            describe('on releasing', function(){
                var handler
                beforeEach(function(){
                    for(var call = 0; call < instances.window.document.addEventListener.calls.count(); call++) {
                        if(instances.window.document.addEventListener.calls.argsFor(call)[0] == 'keyup')
                            handler = instances.window.document.addEventListener.calls.argsFor(call)[1]
                    }
                })                
                
                it('w it centres the left stick vertically', function(){
                    handler({keyCode: 87})
                    expect(part.leftStick).toEqual([0.3, 0.0])
                    expect(part.rightStick).toEqual([0.7, 0.8])                    
                })
                
                it('s it centres the left stick vertically', function(){
                    handler({keyCode: 83})
                    expect(part.leftStick).toEqual([0.3, 0.0])
                    expect(part.rightStick).toEqual([0.7, 0.8])                     
                })          
                
                it('a it centres the left stick horizontally', function(){
                    handler({keyCode: 65})
                    expect(part.leftStick).toEqual([0.0, -0.4])
                    expect(part.rightStick).toEqual([0.7, 0.8])                      
                })            
                
                it('d it centres the left stick horizontally', function(){
                    handler({keyCode: 68})
                    expect(part.leftStick).toEqual([0.0, -0.4])
                    expect(part.rightStick).toEqual([0.7, 0.8])                     
                })     
                
                it('i it centres the left stick vertically', function(){
                    handler({keyCode: 73})
                    expect(part.leftStick).toEqual([0.3, -0.4])
                    expect(part.rightStick).toEqual([0.7, 0.0])                    
                })
                
                it('k it centres the left stick vertically', function(){
                    handler({keyCode: 75})
                    expect(part.leftStick).toEqual([0.3, -0.4])
                    expect(part.rightStick).toEqual([0.7, 0.0])                     
                })          
                
                it('j it centres the left stick horizontally', function(){
                    handler({keyCode: 74})
                    expect(part.leftStick).toEqual([0.3, -0.4])
                    expect(part.rightStick).toEqual([0.0, 0.8])                      
                })            
                
                it('l it centres the left stick horizontally', function(){
                    handler({keyCode: 76})
                    expect(part.leftStick).toEqual([0.3, -0.4])
                    expect(part.rightStick).toEqual([0.0, 0.8])                     
                })                     
                
                it('other keys do nothing', function(){
                    handler({keyCode: 45})
                    expect(part.leftStick).toEqual([0.3, -0.4])
                    expect(part.rightStick).toEqual([0.7, 0.8])                      
                })            
            }) 
        })
    })
})