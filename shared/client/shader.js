parts.shader = function(instances) {
    return function(type, source, filename) {
        var id = instances.canvas.context.createShader(type)
        instances.canvas.context.shaderSource(id, source)
        instances.canvas.context.compileShader(id)
        if(instances.canvas.context.getShaderParameter(id, instances.canvas.context.COMPILE_STATUS))
            return id
        instances.errorReporter('Error compiling shader "' + filename + '": "' + instances.canvas.context.getShaderInfoLog(id) + '"')                
        instances.canvas.context.deleteShader(id)
    }
}