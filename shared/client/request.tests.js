describe('request', function(){
    var part, instances
    beforeEach(function(){
        part = parts.request(instances = {
            
        })
    })
    
    it('takes a reference to the XMLHttpRequest constructor', function(){
        expect(part.XMLHttpRequest).toBe(XMLHttpRequest)
    })
    
    describe('on calling without a body and a callback', function(){
        var callback, request
        beforeEach(function(){
            part.XMLHttpRequest = jasmine.createSpy()
            part.XMLHttpRequest.and.returnValue(request = {
                open: jasmine.createSpy(),
                send: jasmine.createSpy()
            })
            callback = jasmine.createSpy()
        })
        
        it('creates an instance of the request', function(){
            part('Test URL', 'Test Method', callback)
            expect(part.XMLHttpRequest.calls.count()).toEqual(1)
        })
        
        it('does not execute the callback', function(){
            part('Test URL', 'Test Method', callback)
            expect(callback).not.toHaveBeenCalled()
        })
        
        it('registers a callback', function(){
            part('Test URL', 'Test Method', callback)
            expect(request.onreadystatechange).toBeDefined()
        })
        
        it('then opens the request', function(){
            request.open.and.callFake(function(){
                expect(request.onreadystatechange).toBeDefined()
            })
            part('Test URL', 'Test Method', callback)
            expect(request.open).toHaveBeenCalledWith('Test Method', 'Test URL', true)
            expect(request.open.calls.count()).toEqual(1)
        })
        
        it('then sends the request', function(){
            request.send.and.callFake(function(){
                expect(request.open).toHaveBeenCalled()
            })
            part('Test URL', 'Test Method', callback)
            expect(request.send.calls.count()).toEqual(1)            
        })
        
        describe('on a state change other than complete', function(){
            beforeEach(function(){
                part('Test URL', 'Test Method', callback)
                request.readyState = 3
                request.onreadystatechange()
            })
            
            it('does not execute the callback', function(){
                expect(callback).not.toHaveBeenCalled()
            })
        })
        
        describe('on a state change to the complete status', function(){
            beforeEach(function(){
                part('Test URL', 'Test Method', callback)
                request.readyState = 4
            })            
            
            describe('on success', function(){
                beforeEach(function(){
                    request.status = 200
                    request.responseText = 'Test Result'
                    request.onreadystatechange()
                })
                
                it('executes the callback with the result', function(){
                    expect(callback.calls.count()).toEqual(1)
                    expect(callback).toHaveBeenCalledWith('Test Result')
                })
            })
            
            describe('on internal server error', function(){
                beforeEach(function(){
                    instances.errorReporter = jasmine.createSpy()
                    request.status = 500
                    request.responseText = 'Test Error'
                    request.onreadystatechange()
                })
                
                it('does not execute the callback', function(){
                    expect(callback).not.toHaveBeenCalled()
                })
                
                it('reports the error', function(){
                    expect(instances.errorReporter.calls.count()).toEqual(1)
                    expect(instances.errorReporter).toHaveBeenCalledWith('Failed to "Test Method" "Test URL".  (status code 500)')
                })
            })
            
            describe('on bad request', function(){
                beforeEach(function(){
                    instances.errorReporter = jasmine.createSpy()
                    request.status = 400
                    request.responseText = 'Test Error'
                    request.onreadystatechange()
                })
                
                it('does not execute the callback', function(){
                    expect(callback).not.toHaveBeenCalled()
                })
                
                it('reports the error', function(){
                    expect(instances.errorReporter.calls.count()).toEqual(1)
                    expect(instances.errorReporter).toHaveBeenCalledWith('Failed to "Test Method" "Test URL".  (status code 400)')
                })
            })            
            
            describe('on file not found', function(){
                beforeEach(function(){
                    instances.errorReporter = jasmine.createSpy()
                    request.status = 404
                    request.responseText = 'Test Error'
                    request.onreadystatechange()
                })
                
                it('does not execute the callback', function(){
                    expect(callback).not.toHaveBeenCalled()
                })
                
                it('reports the error', function(){
                    expect(instances.errorReporter.calls.count()).toEqual(1)
                    expect(instances.errorReporter).toHaveBeenCalledWith('Failed to "Test Method" "Test URL".  (status code 404)')
                })
            })            
        })
    })
    
    describe('on calling with a body and a callback', function(){
        var callback, request
        beforeEach(function(){
            part.XMLHttpRequest = jasmine.createSpy()
            part.XMLHttpRequest.and.returnValue(request = {
                open: jasmine.createSpy(),
                send: jasmine.createSpy(),
                setRequestHeader: jasmine.createSpy()
            })
            callback = jasmine.createSpy()
        })
        
        it('creates an instance of the request', function(){
            part('Test URL', 'Test Method', callback, 'Test Body')
            expect(part.XMLHttpRequest.calls.count()).toEqual(1)
        })
        
        it('does not execute the callback', function(){
            part('Test URL', 'Test Method', callback, 'Test Body')
            expect(callback).not.toHaveBeenCalled()
        })
        
        it('registers a callback', function(){
            part('Test URL', 'Test Method', callback, 'Test Body')
            expect(request.onreadystatechange).toBeDefined()
        })
        
        it('then opens the request', function(){
            request.open.and.callFake(function(){
                expect(request.onreadystatechange).toBeDefined()
            })
            part('Test URL', 'Test Method', callback, 'Test Body')
            expect(request.open).toHaveBeenCalledWith('Test Method', 'Test URL', true)
            expect(request.open.calls.count()).toEqual(1)
        })
        
        it('then sets the content type', function(){
            request.send.and.callFake(function(){
                expect(request.open).toHaveBeenCalled()
            })
            part('Test URL', 'Test Method', callback, 'Test Body')
            expect(request.setRequestHeader).toHaveBeenCalledWith('content-type', 'application/json')            
        })        
        
        it('then sends the request', function(){
            request.send.and.callFake(function(){
                expect(request.setRequestHeader).toHaveBeenCalled()
            })
            part('Test URL', 'Test Method', callback, 'Test Body')
            expect(request.send.calls.count()).toEqual(1)      
            expect(request.send).toHaveBeenCalledWith('"Test Body"')
        })
        
        describe('on a state change other than complete', function(){
            beforeEach(function(){
                part('Test URL', 'Test Method', callback, 'Test Body')
                request.readyState = 3
                request.onreadystatechange()
            })
            
            it('does not execute the callback', function(){
                expect(callback).not.toHaveBeenCalled()
            })
        })
        
        describe('on a state change to the complete status', function(){
            beforeEach(function(){
                part('Test URL', 'Test Method', callback, 'Test Body')
                request.readyState = 4
            })            
            
            describe('on success', function(){
                beforeEach(function(){
                    request.status = 200
                    request.responseText = 'Test Result'
                    request.onreadystatechange()
                })
                
                it('executes the callback with the result', function(){
                    expect(callback.calls.count()).toEqual(1)
                    expect(callback).toHaveBeenCalledWith('Test Result')
                })
            })
            
            describe('on internal server error', function(){
                beforeEach(function(){
                    instances.errorReporter = jasmine.createSpy()
                    request.status = 500
                    request.responseText = 'Test Error'
                    request.onreadystatechange()
                })
                
                it('does not execute the callback', function(){
                    expect(callback).not.toHaveBeenCalled()
                })
                
                it('reports the error', function(){
                    expect(instances.errorReporter.calls.count()).toEqual(1)
                    expect(instances.errorReporter).toHaveBeenCalledWith('Failed to "Test Method" "Test URL".  (status code 500)')
                })
            })
            
            describe('on bad request', function(){
                beforeEach(function(){
                    instances.errorReporter = jasmine.createSpy()
                    request.status = 400
                    request.responseText = 'Test Error'
                    request.onreadystatechange()
                })
                
                it('does not execute the callback', function(){
                    expect(callback).not.toHaveBeenCalled()
                })
                
                it('reports the error', function(){
                    expect(instances.errorReporter.calls.count()).toEqual(1)
                    expect(instances.errorReporter).toHaveBeenCalledWith('Failed to "Test Method" "Test URL".  (status code 400)')
                })
            })            
            
            describe('on file not found', function(){
                beforeEach(function(){
                    instances.errorReporter = jasmine.createSpy()
                    request.status = 404
                    request.responseText = 'Test Error'
                    request.onreadystatechange()
                })
                
                it('does not execute the callback', function(){
                    expect(callback).not.toHaveBeenCalled()
                })
                
                it('reports the error', function(){
                    expect(instances.errorReporter.calls.count()).toEqual(1)
                    expect(instances.errorReporter).toHaveBeenCalledWith('Failed to "Test Method" "Test URL".  (status code 404)')
                })
            })            
        })
    })    
    
    describe('on calling without a body or callback', function(){
        var request
        beforeEach(function(){
            part.XMLHttpRequest = jasmine.createSpy()
            part.XMLHttpRequest.and.returnValue(request = {
                open: jasmine.createSpy(),
                send: jasmine.createSpy()
            })
        })
        
        it('creates an instance of the request', function(){
            part('Test URL', 'Test Method')
            expect(part.XMLHttpRequest.calls.count()).toEqual(1)
        })
        
        it('registers a callback', function(){
            part('Test URL', 'Test Method')
            expect(request.onreadystatechange).toBeDefined()
        })
        
        it('then opens the request', function(){
            request.open.and.callFake(function(){
                expect(request.onreadystatechange).toBeDefined()
            })
            part('Test URL', 'Test Method')
            expect(request.open).toHaveBeenCalledWith('Test Method', 'Test URL', true)
            expect(request.open.calls.count()).toEqual(1)
        })
        
        it('then sends the request', function(){
            request.send.and.callFake(function(){
                expect(request.open).toHaveBeenCalled()
            })
            part('Test URL', 'Test Method')
            expect(request.send.calls.count()).toEqual(1)            
        })
        
        describe('on a state change other than complete', function(){
            beforeEach(function(){
                part('Test URL', 'Test Method')
                request.readyState = 3
                request.onreadystatechange()
            })
        })
        
        describe('on a state change to the complete status', function(){
            beforeEach(function(){
                part('Test URL', 'Test Method')
                request.readyState = 4
            })            
            
            describe('on success', function(){
                beforeEach(function(){
                    request.status = 200
                    request.responseText = 'Test Result'
                    request.onreadystatechange()
                })
                
                it('does nothing', function(){})
            })
            
            describe('on internal server error', function(){
                beforeEach(function(){
                    instances.errorReporter = jasmine.createSpy()
                    request.status = 500
                    request.responseText = 'Test Error'
                    request.onreadystatechange()
                })
                
                it('reports the error', function(){
                    expect(instances.errorReporter.calls.count()).toEqual(1)
                    expect(instances.errorReporter).toHaveBeenCalledWith('Failed to "Test Method" "Test URL".  (status code 500)')
                })
            })
            
            describe('on bad request', function(){
                beforeEach(function(){
                    instances.errorReporter = jasmine.createSpy()
                    request.status = 400
                    request.responseText = 'Test Error'
                    request.onreadystatechange()
                })
                
                it('reports the error', function(){
                    expect(instances.errorReporter.calls.count()).toEqual(1)
                    expect(instances.errorReporter).toHaveBeenCalledWith('Failed to "Test Method" "Test URL".  (status code 400)')
                })
            })            
            
            describe('on file not found', function(){
                beforeEach(function(){
                    instances.errorReporter = jasmine.createSpy()
                    request.status = 404
                    request.responseText = 'Test Error'
                    request.onreadystatechange()
                })
                
                it('reports the error', function(){
                    expect(instances.errorReporter.calls.count()).toEqual(1)
                    expect(instances.errorReporter).toHaveBeenCalledWith('Failed to "Test Method" "Test URL".  (status code 404)')
                })
            })            
        })
    })
    
    describe('on calling with a body but no callback', function(){
        var request
        beforeEach(function(){
            part.XMLHttpRequest = jasmine.createSpy()
            part.XMLHttpRequest.and.returnValue(request = {
                open: jasmine.createSpy(),
                send: jasmine.createSpy(),
                setRequestHeader: jasmine.createSpy()
            })
        })
        
        it('creates an instance of the request', function(){
            part('Test URL', 'Test Method', null, 'Test Body')
            expect(part.XMLHttpRequest.calls.count()).toEqual(1)
        })
        
        it('registers a callback', function(){
            part('Test URL', 'Test Method', null, 'Test Body')
            expect(request.onreadystatechange).toBeDefined()
        })
        
        it('then opens the request', function(){
            request.open.and.callFake(function(){
                expect(request.onreadystatechange).toBeDefined()
            })
            part('Test URL', 'Test Method', null, 'Test Body')
            expect(request.open).toHaveBeenCalledWith('Test Method', 'Test URL', true)
            expect(request.open.calls.count()).toEqual(1)
        })
        
        it('then sets the content type', function(){
            request.send.and.callFake(function(){
                expect(request.open).toHaveBeenCalled()
            })
            part('Test URL', 'Test Method', null, 'Test Body')
            expect(request.setRequestHeader).toHaveBeenCalledWith('content-type', 'application/json')            
        })        
        
        it('then sends the request', function(){
            request.send.and.callFake(function(){
                expect(request.setRequestHeader).toHaveBeenCalled()
            })
            part('Test URL', 'Test Method', null, 'Test Body')
            expect(request.send.calls.count()).toEqual(1)      
            expect(request.send).toHaveBeenCalledWith('"Test Body"')
        })
        
        describe('on a state change other than complete', function(){
            beforeEach(function(){
                part('Test URL', 'Test Method', null, 'Test Body')
                request.readyState = 3
                request.onreadystatechange()
            })
        })
        
        describe('on a state change to the complete status', function(){
            beforeEach(function(){
                part('Test URL', 'Test Method', null, 'Test Body')
                request.readyState = 4
            })            
            
            describe('on success', function(){
                beforeEach(function(){
                    request.status = 200
                    request.responseText = 'Test Result'
                    request.onreadystatechange()
                })
                
                it('does nothing', function(){})
            })
            
            describe('on internal server error', function(){
                beforeEach(function(){
                    instances.errorReporter = jasmine.createSpy()
                    request.status = 500
                    request.responseText = 'Test Error'
                    request.onreadystatechange()
                })
                
                it('reports the error', function(){
                    expect(instances.errorReporter.calls.count()).toEqual(1)
                    expect(instances.errorReporter).toHaveBeenCalledWith('Failed to "Test Method" "Test URL".  (status code 500)')
                })
            })
            
            describe('on bad request', function(){
                beforeEach(function(){
                    instances.errorReporter = jasmine.createSpy()
                    request.status = 400
                    request.responseText = 'Test Error'
                    request.onreadystatechange()
                })
                
                it('reports the error', function(){
                    expect(instances.errorReporter.calls.count()).toEqual(1)
                    expect(instances.errorReporter).toHaveBeenCalledWith('Failed to "Test Method" "Test URL".  (status code 400)')
                })
            })            
            
            describe('on file not found', function(){
                beforeEach(function(){
                    instances.errorReporter = jasmine.createSpy()
                    request.status = 404
                    request.responseText = 'Test Error'
                    request.onreadystatechange()
                })
                
                it('reports the error', function(){
                    expect(instances.errorReporter.calls.count()).toEqual(1)
                    expect(instances.errorReporter).toHaveBeenCalledWith('Failed to "Test Method" "Test URL".  (status code 404)')
                })
            })            
        })
    })    
})