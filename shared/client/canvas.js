/* On calling, finds the canvas and opens a WebGL context through it, storing:
   -canvas - a reference to the canvas.
   -context - a reference to the WebGL context.
   */
parts.canvas = function(instances) {
    var part
    return part = function(){
        part.canvas = instances.window.document.getElementsByTagName('canvas')[0]
        part.context = part.canvas.getContext('webgl') || part.canvas.getContext('experimental-webgl')
        if(!part.context) {
            instances.errorReporter(instances.localizations.noWebGl)
            return
        }
    }
}