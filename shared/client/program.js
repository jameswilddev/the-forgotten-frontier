/* Call with the GLSL vertex source, fragment source and a filename to use for
   error reporting to create a shader program.  (the id is returned)  */
parts.program = function(instances){
    return function(vertexSource, fragmentSource, filename) {
        var gl = instances.canvas.context
        var vertex = instances.shader(gl.VERTEX_SHADER, vertexSource, filename + ' (vertex shader)')
        if(!vertex) return
        var fragment = instances.shader(gl.FRAGMENT_SHADER, fragmentSource, filename + ' (fragment shader)')
        if(!fragment) {
            gl.deleteShader(vertex)
            return
        }
        var program = gl.createProgram()
        gl.attachShader(program, vertex)
        gl.attachShader(program, fragment)
        
        gl.linkProgram(program)
        
        gl.detachShader(program, fragment)
        gl.deleteShader(fragment)
        gl.detachShader(program, vertex)
        gl.deleteShader(vertex)
        
        if(gl.getProgramParameter(program, gl.LINK_STATUS)) {
            return program
        } else  {
            instances.errorReporter('Failed to link shader for "' + filename + '": "' + gl.getProgramInfoLog(program) + '"')
            gl.deleteProgram(program)
        }
    }
}