/* On calling, sets up an event loop, setting the following properties:
   - tick is the subscribe function of an event fired at ticksPerSecond from configuration.
   - render is the subscribe function of an event fired every frame.
   - time is the number of seconds since the start of the event loop.
   - width/height are the width and height of the viewport respectively.
      The canvas will have been resized and the WebGL viewport set up to fill
      ths automatically by the time the tick/render events are raised.
   - progress is the normalized (0...1) progress through the current tick. */
parts.eventLoop = function(instances) {
    var part
    return part = function() {
        var tick = instances.timer(instances.configuration.ticksPerSecond)
        part.tick = tick.subscribe
        
        var render = instances.event()
        part.render = render.subscribe        
        
        var previous = 0, callback
        instances.window.requestAnimationFrame(callback = function(time){
            part.time = time / 1000
            
            part.width = instances.window.innerWidth
            part.height = instances.window.innerHeight
            
            instances.canvas.canvas.width = part.width
            instances.canvas.canvas.height = part.height
            instances.canvas.context.viewport(0, 0, part.width, part.height)
            
            part.framerate = Math.round(1000 / (time - previous))
            tick.advance(Math.min((time - previous) / 1000, instances.configuration.maximumDelta))
            part.progress = tick.progress
            previous = time
            render.raise()

            instances.window.requestAnimationFrame(callback)
        })        
    }
}