describe('canvas', function(){
    var instances, part
    beforeEach(function(){
        part = parts.canvas(instances = {})
    })
    
    describe('on calling', function(){
        var canvas, context
        beforeEach(function(){
            context = {}
            canvas = {
                getContext: jasmine.createSpy()
            }
            instances.window = {
                document: {}
            }
            instances.window.document.getElementsByTagName = jasmine.createSpy()
            instances.window.document.getElementsByTagName.and.callFake(function(tagName){
                if(tagName == 'canvas') return [canvas]
            })
        })
        
        it('shows an error message when no context is available', function(){
            instances.errorReporter = jasmine.createSpy()
            instances.localizations = {
                noWebGl: 'Test Message'
            }
            part()
            expect(instances.errorReporter.calls.count()).toEqual(1)
            expect(instances.errorReporter).toHaveBeenCalledWith('Test Message')
        })
        
        describe('when the context is available', function(){
            var timer, event
            beforeEach(function(){
                canvas.getContext.and.callFake(function(contextType){
                    if(contextType == 'webgl') return context
                })                
                part()
            })
            
            it('copies the canvas to a property', function(){
                expect(part.canvas).toBe(canvas)
            })            
            
            it('finds the context and stores it', function(){
                expect(canvas.getContext).toHaveBeenCalledWith('webgl')
                expect(part.context).toBe(context)
            })
        })
        
        describe('when the context is available in experimental mode', function(){
            var timer, event
            beforeEach(function(){
                canvas.getContext.and.callFake(function(contextType){
                    if(contextType == 'experimental-webgl') return context
                })                
                part()
            })
            
            it('copies the canvas to a property', function(){
                expect(part.canvas).toBe(canvas)
            })              
            
            it('finds the context and stores it', function(){
                expect(canvas.getContext).toHaveBeenCalledWith('experimental-webgl')
                expect(part.context).toBe(context)
            })
        })        
    })
})