/* Given an error message, shows that error to the user.  Later versions may
   attempt to report the error to the analytics service. */
parts.errorReporter = function(instances){
    return function(message) {
        instances.window.alert(instances.localizations.errorPreamble + message)
        if(instances.analytics)
            instances.analytics.error(message)
    }
}