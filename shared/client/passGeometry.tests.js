describe('passGeometry', function(){
    var part, instances
    beforeEach(function(){
        part = parts.passGeometry(instances = {})
    })
    describe('first call', function(){
        beforeEach(function(){
            instances.canvas = {
                context: {
                    ARRAY_BUFFER: 'Test Array Buffer',
                    createBuffer: jasmine.createSpy(),
                    bindBuffer: jasmine.createSpy(),
                    bufferData: jasmine.createSpy(),
                    STATIC_DRAW: 'Test Static Draw',
                    TRIANGLE_STRIP: 'Test Triangle Strip',
                    vertexAttribPointer: jasmine.createSpy(),
                    enableVertexAttribArray: jasmine.createSpy(),
                    disableVertexAttribArray: jasmine.createSpy(),
                    drawArrays: jasmine.createSpy(),
                    FLOAT: 'Test Float'
                }
            }
            instances.canvas.context.createBuffer.and.returnValue('Test Buffer Id')
        })

        it('creates an array buffer', function(){
            part()
            expect(instances.canvas.context.createBuffer.calls.count()).toEqual(1)
            expect(instances.canvas.context.createBuffer).toHaveBeenCalledWith('Test Array Buffer')
        })

        it('binds the array buffer', function(){
            part()
            expect(instances.canvas.context.bindBuffer.calls.count()).toEqual(1)
            expect(instances.canvas.context.bindBuffer).toHaveBeenCalledWith('Test Array Buffer', 'Test Buffer Id')
        })

        it('then copies the vertices', function(){
            instances.canvas.context.bufferData.and.callFake(function(){
                expect(instances.canvas.context.bindBuffer).toHaveBeenCalled()
            })
            part()
            expect(instances.canvas.context.bufferData.calls.count()).toEqual(1)
            expect(instances.canvas.context.bufferData.calls.argsFor(0)[0]).toEqual('Test Array Buffer')
            expect(instances.canvas.context.bufferData.calls.argsFor(0)[1] instanceof Float32Array).toBeTruthy()
            expect(Array.prototype.slice.call(instances.canvas.context.bufferData.calls.argsFor(0)[1])).toEqual([
                -1.0, -1.0,
                1.0, -1.0,
                -1.0, 1.0,
                1.0, 1.0
            ])
            expect(instances.canvas.context.bufferData.calls.argsFor(0)[2]).toEqual('Test Static Draw')
        })

        it('then enables the vertex array', function(){
            instances.canvas.context.enableVertexAttribArray.and.callFake(function(){
                expect(instances.canvas.context.bufferData).toHaveBeenCalled()
            })
            part('Test Position Id')
            expect(instances.canvas.context.enableVertexAttribArray.calls.count()).toEqual(1)
            expect(instances.canvas.context.enableVertexAttribArray.calls.argsFor(0)[0]).toEqual('Test Position Id')
        })

        it('then configures the vertex array', function(){
            instances.canvas.context.vertexAttribPointer.and.callFake(function(){
                expect(instances.canvas.context.enableVertexAttribArray).toHaveBeenCalled()
            })
            part('Test Position Id')
            expect(instances.canvas.context.vertexAttribPointer.calls.count()).toEqual(1)
            expect(instances.canvas.context.vertexAttribPointer.calls.argsFor(0)).toEqual(['Test Position Id', 2, 'Test Float', false, 0, 0])
        })

        it('then draws the geometry once all of this configuration has completed', function(){
            instances.canvas.context.drawArrays.and.callFake(function(){
                expect(instances.canvas.context.vertexAttribPointer).toHaveBeenCalled()
            })
            part()
            expect(instances.canvas.context.drawArrays.calls.count()).toEqual(1)
            expect(instances.canvas.context.drawArrays.calls.argsFor(0)).toEqual(['Test Triangle Strip', 0, 4])
        })

        it('disables the vertex array after drawing', function(){
            instances.canvas.context.disableVertexAttribArray.and.callFake(function(){
                expect(instances.canvas.context.drawArrays).toHaveBeenCalled()
            })
            part('Test Position Id')
            expect(instances.canvas.context.disableVertexAttribArray.calls.count()).toEqual(1)
            expect(instances.canvas.context.disableVertexAttribArray.calls.argsFor(0)[0]).toEqual('Test Position Id')
        })

        describe('subsequent call', function(){
            beforeEach(function(){
                part()
            })
            it('does not create a new buffer', function(){
                part()
                expect(instances.canvas.context.createBuffer.calls.count()).toEqual(1)
            })

            it('does not change the contents of the buffer', function(){
                part()
                expect(instances.canvas.context.bufferData.calls.count()).toEqual(1)
            })

            it('binds the array buffer', function(){
                part()
                expect(instances.canvas.context.bindBuffer.calls.count()).toEqual(2)
                expect(instances.canvas.context.bindBuffer.calls.argsFor(1)).toEqual(['Test Array Buffer', 'Test Buffer Id'])
            })

            it('enables the vertex array', function(){
                instances.canvas.context.enableVertexAttribArray.and.callFake(function(){
                    expect(instances.canvas.context.bindBuffer.calls.count()).toEqual(2)
                })
                part('Test Position 2')
                expect(instances.canvas.context.enableVertexAttribArray.calls.count()).toEqual(2)
                expect(instances.canvas.context.enableVertexAttribArray.calls.argsFor(1)[0]).toEqual('Test Position 2')                                          
            })

            it('configures the vertex array', function(){
                instances.canvas.context.vertexAttribPointer.and.callFake(function(){
                    expect(instances.canvas.context.enableVertexAttribArray.calls.count()).toEqual(2)
                })
                part('Test Position 2')
                expect(instances.canvas.context.vertexAttribPointer.calls.count()).toEqual(2)
                expect(instances.canvas.context.vertexAttribPointer.calls.argsFor(1)).toEqual(['Test Position 2', 2, 'Test Float', false, 0, 0])
            })

            it('draws the geometry once all of this configuration has completed', function(){
                instances.canvas.context.drawArrays.and.callFake(function(){
                    expect(instances.canvas.context.vertexAttribPointer.calls.count()).toEqual(2)
                })
                part()
                expect(instances.canvas.context.drawArrays.calls.count()).toEqual(2)
                expect(instances.canvas.context.drawArrays.calls.argsFor(1)).toEqual(['Test Triangle Strip', 0, 4])
            })

            it('disables the vertex array after drawing', function(){
                instances.canvas.context.disableVertexAttribArray.and.callFake(function(){
                    expect(instances.canvas.context.drawArrays.calls.count()).toEqual(2)
                })
                part('Test Position 2')
                expect(instances.canvas.context.disableVertexAttribArray.calls.count()).toEqual(2)
                expect(instances.canvas.context.disableVertexAttribArray.calls.argsFor(1)[0]).toEqual('Test Position 2')
            })
        })
    })
})