// Call to get a new event.
// An event is an object with two functions:
// - subscribe, which is given a callback to add to the event.
// - raise, which executes all subscribed events.
// To unsubscribe, execute the function given as an argument to the callback
// and returned from the call to subscribe.
parts.event = function(){
    return function(){
        var callbacks = []
        return {
            subscribe: function(callback){
                var unsubscribe
                var wrapped = function(){
                    callback(unsubscribe)
                }
                callbacks.push(wrapped)
                unsubscribe = function(){
                    var index = callbacks.indexOf(wrapped)
                    if(index == -1) return
                    callbacks.splice(index, 1)
                }
                return unsubscribe
            },
            raise: function(){
                for(var index = 0; index < callbacks.length; index++)
                    callbacks[index]()
            }
        }
    }
}