/* A vector maths part.
   Exposes add/subtract/multiply/divide/dot/cross functions taking arrays of
   floats.  Should a function need to return a vector, the result is written to 
   an extra parameter rather than being returned to reduce GC load.  Usually,
   single item arrays can be used as scalars.  Usually, input vectors may be
   reused as output vectors too.
   - add, subtract, multiply and divide take two vectors and an output vector.
   - dot takes two vectors and returns the product.
   - cross takes two 3-axis vectors and an output vector.
   - magitude and magnitudeSquared take a single vector and return the magnitude.
   - normalize takes an input vector and an output vector.
   - interpolate takes a vector to interpolate from, a vector to interpolate to,
     a vector containing the alpha, and an output vector.
   - distance and distanceSquared take two vectors and return the distance.
   - reflect takes a vector, a surface normal and an output vector.
*/
parts.vector = function(){
    var zip = function(a, b, output, callback) {
        for(var index = 0; index < Math.max(a.length, b.length); index++)
            output[index] = callback(a[Math.min(index, a.length - 1)], b[Math.min(index, b.length - 1)])        
    }
    var part
    return part = {
        add: function(a, b, output) {
            zip(a, b, output, function(a, b){ return a + b })
        },
        subtract: function(a, b, output) {
            zip(a, b, output, function(a, b){ return a - b })
        },
        multiply: function(a, b, output) {
            zip(a, b, output, function(a, b){ return a * b })
        },  
        divide: function(a, b, output) {
            zip(a, b, output, function(a, b){ return a / b })
        }, 
        dot: function(a, b) {
            var sum = 0
            for(var index = 0; index < a.length; index++)
                sum += a[index] * b[index]
            return sum
        },
        cross: function(a, b, output) {
            output[0] = a[1] * b[2] - b[1] * a[2]
            output[1] = a[2] * b[0] - b[2] * a[0]
            output[2] = a[0] * b[1] - b[0] * a[1]
        },
        magnitudeSquared: function(a) {
            var sum = 0
            for(var index = 0; index < a.length; index++)
                sum += a[index] * a[index]
            return sum
        },
        magnitude: function(a) {
            var sum = 0
            for(var index = 0; index < a.length; index++)
                sum += a[index] * a[index]
            return Math.sqrt(sum)
        },
        normalize: function(input, output) {
            var length = part.magnitude(input)
            for(var index = 0; index < input.length; index++)
                output[index] = input[index] / length
        },
        interpolate: function(from, to, progress, output) {
            var len = Math.max(from.length, to.length, progress.length)
            for(var index = 0; index < len; index++)
                output[index] = from[Math.min(index, from.length - 1)] * (1.0 - progress[Math.min(index, progress.length - 1)])
                    + to[Math.min(index, to.length - 1)] * progress[Math.min(index, progress.length - 1)]                      
        },
        distanceSquared: function(a, b) {
            var sum = 0
            for(var index = 0; index < a.length; index++) {
                var pair = a[index] - b[index]
                sum += pair * pair
            }
            return sum
        },
        distance: function(a, b) {
            var sum = 0
            for(var index = 0; index < a.length; index++) {
                var pair = a[index] - b[index]
                sum += pair * pair
            }
            return Math.sqrt(sum)
        },
        reflect: function(a, normal, output) {
            var dot = part.dot(a, normal) * 2.0
            zip(a, normal, output, function(a, b){ return a - b * dot })
        },
        clone: function(input, output) {
            output.length = input.length
            for(var component = 0; component < input.length; component++)
                output[component] = input[component]
        }
    }
}