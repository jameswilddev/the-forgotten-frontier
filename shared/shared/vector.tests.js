describe('vector', function(){
    var vector
    beforeEach(function(){
        vector = parts.vector()
    })
    describe('add', function(){
        it('vector to vector', function(){
            var result = []
            vector.add([-8.5, 1], [8, 10], result)
            expect(result).toEqual([-0.5, 11])
        })
        it('vector by scalar', function(){
            var result = []
            vector.add([9, 3, 2], [3], result)
            expect(result).toEqual([12, 6, 5])
        })  
        it('vector to scalar', function(){
            var result = []
            vector.add([6], [-8, 2, -3], result)
            expect(result).toEqual([-2, 8, 3])
        }) 
        it('scalar to scalar', function(){
            var result = []
            vector.add([3], [8], result)
            expect(result).toEqual([11])
        })
        it('does not modify the inputs', function(){
            var inputA = [1, 2, 3], inputB = [4, 5, 6]
            vector.add(inputA, inputB, [])
            expect(inputA).toEqual([1, 2, 3])
            expect(inputB).toEqual([4, 5, 6])
        })
        it('supports having the first argument as the output', function(){
            var result = [3, 5]
            vector.add(result, [5, -9], result)
            expect(result).toEqual([8, -4])            
        })
        it('supports having the second argument as the output', function(){
            var result = [3, 5]
            vector.add([5, -9], result, result)
            expect(result).toEqual([8, -4])
        })        
        it('supports having both arguments as the output', function(){
            var result = [3, 5]
            vector.add(result, result, result)
            expect(result).toEqual([6, 10])
        })        
    })
    describe('subtract', function(){
        it('vector by vector', function(){
            var result = []
            vector.subtract([3, -9], [-1.9, -4.3], result)
            expect(result).toEqual([4.9, -4.7])
        })
        it('vector by scalar', function(){
            var result = []
            vector.subtract([8.4, 2.9, 1.4], [4], result)
            expect(result).toEqual([4.4, -1.1, -2.6])
        })  
        it('vector from scalar', function(){
            var result = []
            vector.subtract([3], [2, -4, 7], result)
            expect(result).toEqual([1, 7, -4])
        }) 
        it('scalar by scalar', function(){
            var result = []
            vector.subtract([9], [4], result)
            expect(result).toEqual([5])            
        })        
        it('does not modify the inputs', function(){
            var inputA = [1, 2, 3], inputB = [4, 5, 6]
            vector.subtract(inputA, inputB, [])
            expect(inputA).toEqual([1, 2, 3])
            expect(inputB).toEqual([4, 5, 6])            
        })
        it('supports having the first argument as the output', function(){
            
        })
        it('supports having the second argument as the output', function(){
            
        })        
        it('supports having both arguments as the output', function(){
            
        })          
    }) 
    describe('multiply', function(){
        it('vector by vector', function(){
            var result = []
            vector.multiply([4, -2], [8, -3], result)
            expect(result).toEqual([32, 6])
        })
        it('vector by scalar', function(){
            var result = []
            vector.multiply([5, 8, -2], [4], result)
            expect(result).toEqual([20, 32, -8])
        })  
        it('vector from scalar', function(){
            var result = []
            vector.multiply([2], [-6, 3, 7], result)
            expect(result).toEqual([-12, 6, 14])
        })   
        it('scalar by scalar', function(){
            var result = []
            vector.multiply([9], [4], result)
            expect(result).toEqual([36])                        
        })                
        it('does not modify the inputs', function(){
            var inputA = [1, 2, 3], inputB = [4, 5, 6]
            vector.multiply(inputA, inputB, [])
            expect(inputA).toEqual([1, 2, 3])
            expect(inputB).toEqual([4, 5, 6])            
        })
        it('supports having the first argument as the output', function(){
            
        })
        it('supports having the second argument as the output', function(){
            
        })        
        it('supports having both arguments as the output', function(){
            
        })         
    })   
    describe('divide', function(){
        it('vector by vector', function(){
            var result = []
            vector.divide([15, 16], [5, 8], result)
            expect(result).toEqual([3, 2])
        })
        it('vector by scalar', function(){
            var result = []
            vector.divide([8, 40, -22], [2], result)
            expect(result).toEqual([4, 20, -11])
        })  
        it('vector from scalar', function(){
            var result = []
            vector.divide([7], [56, -700, 35], result)
            expect(result).toEqual([0.125, -0.01, 0.2])
        })  
        it('scalar by scalar', function(){
            var result = []
            vector.divide([8], [4], result)
            expect(result).toEqual([2])                                    
        })                
        it('does not modify the inputs', function(){
            var inputA = [1, 2, 3], inputB = [4, 5, 6]
            vector.divide(inputA, inputB, [])
            expect(inputA).toEqual([1, 2, 3])
            expect(inputB).toEqual([4, 5, 6])            
        })
        it('supports having the first argument as the output', function(){
            
        })
        it('supports having the second argument as the output', function(){
            
        })        
        it('supports having both arguments as the output', function(){
            
        })         
    })  
    describe('dot', function(){
        it('calculates the dot product', function(){
            expect(vector.dot([3, -6], [8, 9])).toEqual(-30)
        })
        it('does not modify the inputs', function(){
            var inputA = [1, 2, 3], inputB = [4, 5, 6]
            vector.dot(inputA, inputB)
            expect(inputA).toEqual([1, 2, 3])
            expect(inputB).toEqual([4, 5, 6])            
        })
    })
    describe('cross', function(){
        it('calculates the cross product', function(){
            var result = []
            vector.cross([3, 5, 7], [4, 6, 7], result)
            expect(result).toEqual([-7, 7, -2])
        })
        it('does not modify the inputs', function(){
            var inputA = [1, 2, 3], inputB = [4, 5, 6]
            vector.cross(inputA, inputB, [])
            expect(inputA).toEqual([1, 2, 3])
            expect(inputB).toEqual([4, 5, 6])            
        })
        it('supports having the first argument as the output', function(){
            
        })
        it('supports having the second argument as the output', function(){
            
        })        
        it('supports having both arguments as the output', function(){
            
        })         
    })
    describe('magnitudeSquared', function(){
        it('calculates the square of the magnitude', function(){
            expect(vector.magnitudeSquared([-7, 4, 2])).toEqual(69)
        })
        it('does not modify the inputs', function(){
            var input = [1, 2, 3]
            vector.magnitudeSquared(input)
            expect(input).toEqual([1, 2, 3])
        })
    })
    describe('magnitude', function(){
        it('calculates the magnitude', function(){
            expect(vector.magnitude([8, -5, -2])).toBeCloseTo(9.64365);
        })
        it('does not modify the inputs', function(){
            var input = [1, 2, 3]
            vector.magnitude(input)
            expect(input).toEqual([1, 2, 3])            
        })
    })
    describe('normalize', function(){
        it('with separate input and output arrays', function(){
            var input = [9, -3, 5]
            var result = []
            vector.normalize(input, result)
            expect(input).toEqual([9, -3, 5])
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(0.839254)
            expect(result[1]).toBeCloseTo(-0.27951)
            expect(result[2]).toBeCloseTo(0.466252)
        })
        it('with the same array for input and output', function(){
            var result = [9, -3, 5]
            vector.normalize(result, result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(0.839254)
            expect(result[1]).toBeCloseTo(-0.27951)
            expect(result[2]).toBeCloseTo(0.466252)
        })
    })
    describe('interpolate', function(){
        it('from vector to vector by scalar', function(){
            var result = []
            vector.interpolate([4, -8, 10], [16, -4, -8], [0.8], result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(13.6)
            expect(result[1]).toBeCloseTo(-4.8)
            expect(result[2]).toBeCloseTo(-4.4)       
        })
        it('from scalar to vector by scalar', function(){
            var result = []
            vector.interpolate([4], [16, -4, -8], [0.8], result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(13.6)
            expect(result[1]).toBeCloseTo(-2.4)
            expect(result[2]).toBeCloseTo(-5.6)       
        })
        it('from vector to scalar by scalar', function(){
            var result = []
            vector.interpolate([4, -8, 10], [16], [0.8], result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(13.6)
            expect(result[1]).toBeCloseTo(11.2)
            expect(result[2]).toBeCloseTo(14.8)       
        })   
        it('from scalar to scalar by scalar', function(){
            var result = []
            vector.interpolate([4], [16], [0.8], result)
            expect(result.length).toEqual(1)
            expect(result[0]).toBeCloseTo(13.6)  
        })           
        it('from vector to vector by vector', function(){
            var result = []
            vector.interpolate([4, -8, 10], [16, -4, -8], [0.8, 0.2, 0.5], result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(13.6)
            expect(result[1]).toBeCloseTo(-7.2)
            expect(result[2]).toBeCloseTo(1)       
        })         
        it('from vector to scalar by vector', function(){
            var result = []
            vector.interpolate([4, -8, 10], [16], [0.8, 0.2, 0.5], result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(13.6)
            expect(result[1]).toBeCloseTo(-3.2)
            expect(result[2]).toBeCloseTo(13)       
        })  
        it('from scalar to vector by vector', function(){
            var result = []
            vector.interpolate([4], [16, -4, -8], [0.8, 0.2, 0.5], result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(13.6)
            expect(result[1]).toBeCloseTo(2.4)
            expect(result[2]).toBeCloseTo(-2)       
        })         
        it('from scalar to scalar by vector', function(){
            var result = []
            vector.interpolate([4], [16], [0.8, 0.2, 0.5], result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(13.6)
            expect(result[1]).toBeCloseTo(6.4)
            expect(result[2]).toBeCloseTo(10)
        })          
        it('does not modify the inputs', function(){
            var inputA = [3, 5, 7], inputB = [4, 9, 4], inputC = [2, 7, 3]
            vector.interpolate(inputA, inputB, inputC, [])
            expect(inputA).toEqual([3, 5, 7])
            expect(inputB).toEqual([4, 9, 4])
            expect(inputC).toEqual([2, 7, 3])
        })
        it('supports having the first argument as the output', function(){
            var result = [4, -8, 10]
            vector.interpolate(result, [16, -4, -8], [0.8, 0.2, 0.5], result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(13.6)
            expect(result[1]).toBeCloseTo(-7.2)
            expect(result[2]).toBeCloseTo(1)   
        })
        it('supports having the second argument as the output', function(){
            var result = [16, -4, -8]
            vector.interpolate([4, -8, 10], result, [0.8, 0.2, 0.5], result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(13.6)
            expect(result[1]).toBeCloseTo(-7.2)
            expect(result[2]).toBeCloseTo(1)   
        })       
        it('supports having the third argument as the output', function(){
            var result = [0.8, 0.2, 0.5]
            vector.interpolate([4, -8, 10], [16, -4, -8], result, result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(13.6)
            expect(result[1]).toBeCloseTo(-7.2)
            expect(result[2]).toBeCloseTo(1) 
        })            
    })
    describe('distanceSquared', function(){
        it('calculates the square of the distance', function(){
            expect(vector.distanceSquared([7, -4, 5], [-8, 5, 4])).toEqual(307)
        })
        it('does not modify the inputs', function(){
            var inputA = [1, 2, 3], inputB = [4, 5, 6]
            vector.distanceSquared(inputA, inputB)
            expect(inputA).toEqual([1, 2, 3])
            expect(inputB).toEqual([4, 5, 6]) 
        })
    })
    describe('distance', function(){
        it('calculates the distance', function(){
            expect(vector.distance([3, -4, 1], [9.5, -3, -3])).toBeCloseTo(7.6974)
        })
        it('does not modify the inputs', function(){
            var inputA = [1, 2, 3], inputB = [4, 5, 6]
            vector.distance(inputA, inputB)
            expect(inputA).toEqual([1, 2, 3])
            expect(inputB).toEqual([4, 5, 6]) 
        })
    })
    describe('reflect', function(){
        it('head on', function(){
            var result = []
            vector.reflect([-4, 8, 7], [0.35218, -0.704361, -0.616316], result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(4)
            expect(result[1]).toBeCloseTo(-8)
            expect(result[2]).toBeCloseTo(-7)              
        })
        it('surface facing along axis', function(){
            var result = []
            vector.reflect([-9, 8, 3], [0, 0, 1], result)
            expect(result).toEqual([-9, 8, -3])            
        })
        it('45-degree plane', function(){
            var result = []
            vector.reflect([0, 4, -8], [0.707107, -0.707107, 0], result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(4)
            expect(result[1]).toBeCloseTo(0)
            expect(result[2]).toBeCloseTo(-8)                          
        })
        it('does not modify the inputs', function(){
            var inputA = [1, 2, 3], inputB = [4, 5, 6]
            vector.reflect(inputA, inputB, [])
            expect(inputA).toEqual([1, 2, 3])
            expect(inputB).toEqual([4, 5, 6]) 
        })
        it('supports having the first argument as the output', function(){
            var result = [0, 4, -8]
            vector.reflect(result, [0.707107, -0.707107, 0], result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(4)
            expect(result[1]).toBeCloseTo(0)
            expect(result[2]).toBeCloseTo(-8) 
        })
        it('supports having the second argument as the output', function(){
            var result = [0.707107, -0.707107, 0]
            vector.reflect([0, 4, -8], result, result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(4)
            expect(result[1]).toBeCloseTo(0)
            expect(result[2]).toBeCloseTo(-8)             
        })        
        it('supports having both arguments as the output', function(){
            var result = [0.35218, -0.704361, -0.616316]
            vector.reflect(result, result, result)
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(-0.35218)
            expect(result[1]).toBeCloseTo(0.704361)
            expect(result[2]).toBeCloseTo(0.616316)              
        })         
    })
    describe('clone', function(){
        describe('length equal', function(){
            it('copies the items from the input to the output', function(){
                var output = [8, 9, 1]
                vector.clone([3, 6, 7], output)
                expect(output).toEqual([3, 6, 7])
            })
            
            it('does not modify the input', function(){
                var input = [3, 6, 7]
                vector.clone(input, [8, 9, 1])
                expect(input).toEqual([3, 6, 7])
            })
        })
        
        describe('copying to longer', function(){
            it('copies the items from the input to the output', function(){
                var output = [8, 9, 1, 4]
                vector.clone([3, 6, 7], output)
                expect(output).toEqual([3, 6, 7])
            })
            
            it('does not modify the input', function(){
                var input = [3, 6, 7]
                vector.clone(input, [8, 9, 1, 4])
                expect(input).toEqual([3, 6, 7])
            })
        })   
        
        describe('copying to shorter', function(){
            it('copies the items from the input to the output', function(){
                var output = [8, 9]
                vector.clone([3, 6, 7], output)
                expect(output).toEqual([3, 6, 7])
            })
            
            it('does not modify the input', function(){
                var input = [3, 6, 7]
                vector.clone(input, [8, 9])
                expect(input).toEqual([3, 6, 7])
            })
        })         
        
        it('supports input same as output', function(){
            var input = [3, 6, 7]
            vector.clone(input, input)
            expect(input).toEqual([3, 6, 7])
        })
    })
})