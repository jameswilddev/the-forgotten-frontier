describe('actor', function(){
    var part, instances
    beforeEach(function(){
        part = parts.actor(instances = {})
    })
    
    describe('create', function(){
        var result
        beforeEach(function(){
            instances.navmesh = {
                closestTile: jasmine.createSpy(),
                constrainToTile: jasmine.createSpy()
            }
            
            instances.navmesh.closestTile.and.returnValue('Test Tile')
            instances.navmesh.constrainToTile.and.callFake(function(tile, input, output){
                output[0] = 3
                output[1] = 6
                output[2] = 7
            })
            
            instances.vector = parts.vector(instances)
            
            result = part.create('Test Navmesh', 'Test Location')
        })
        
        it('finds the closest tile', function(){
            expect(instances.navmesh.closestTile.calls.count()).toEqual(1)
            expect(instances.navmesh.closestTile).toHaveBeenCalledWith('Test Navmesh', 'Test Location')
        })
        
        it('copies the closest tile to the output', function(){
            expect(result.tile).toEqual('Test Tile')
        })
        
        it('constrains the location to the closest tile', function(){
            expect(instances.navmesh.constrainToTile.calls.count()).toEqual(1)
            expect(instances.navmesh.constrainToTile).toHaveBeenCalledWith('Test Tile', 'Test Location', jasmine.any(Array))
        })
        
        it('copies the constrained location to two distinct properties', function(){
            expect(result.location).toEqual([3, 6, 7])
            expect(result.previousLocation).toEqual([3, 6, 7])
            expect(result.location).not.toBe(result.previousLocation)
        })
    })
    
    describe('collision', function(){
        var actor, navmesh, navmeshBackup
        
        describe('passes through the surface of the navmesh without colliding', function(){
            beforeEach(function(){
                instances.vector = parts.vector(instances)
                instances.plane = parts.plane(instances)
                instances.navmesh = parts.navmesh(instances)
                
                var geometry = {
                    vertices: [
                        [-4, -5, 1],
                        [-2, -5, 1],
                        [-4, -3, 1],
                        [-2, -3, 1],
                        [-4, -1, 1],
                        [-2, -1, 1]
                    ],
                    faces: [
                        [0, 2, 3, 1],
                        [2, 4, 5, 3]
                    ]
                }
                
                navmesh = instances.navmesh.create(geometry)
                navmeshBackup = instances.navmesh.create(geometry)
                
                actor = {
                    location: [-2.94856, -3.40552, -2.0],
                    previousLocation: [-3.12188, -4.08722, 2.0],
                    tile: navmesh[0]
                }
                
                part.collide(actor)
            })
            
            it('does not change location', function(){
                expect(actor.location).toEqual([-2.94856, -3.40552, -2.0])
            })
            
            it('does not change previousLocation', function(){
                expect(actor.previousLocation).toEqual([-3.12188, -4.08722, 2.0])
            })
            
            it('does not change tile', function(){
                expect(actor.tile).toBe(navmesh[0])
            })
            
            it('does not modify the navmesh', function(){
                expect(navmesh).toEqual(navmeshBackup)
            })            
        })
        
        describe('not leaving the current tile', function(){
            beforeEach(function(){
                instances.vector = parts.vector(instances)
                instances.plane = parts.plane(instances)
                instances.navmesh = parts.navmesh(instances)
                
                var geometry = {
                    vertices: [
                        [-4, -5, 1],
                        [-2, -5, 1],
                        [-4, -3, 1],
                        [-2, -3, 1],
                        [-4, -1, 1],
                        [-2, -1, 1]
                    ],
                    faces: [
                        [0, 2, 3, 1],
                        [2, 4, 5, 3]
                    ]
                }
                
                navmesh = instances.navmesh.create(geometry)
                navmeshBackup = instances.navmesh.create(geometry)
                
                actor = {
                    location: [-2.94856, -3.40552, 2.0],
                    previousLocation: [-3.12188, -4.08722, 2.0],
                    tile: navmesh[0]
                }
                
                part.collide(actor)
            })
            
            it('does not change location', function(){
                expect(actor.location).toEqual([-2.94856, -3.40552, 2.0])
            })
            
            it('does not change previousLocation', function(){
                expect(actor.previousLocation).toEqual([-3.12188, -4.08722, 2.0])
            })
            
            it('does not change tile', function(){
                expect(actor.tile).toBe(navmesh[0])
            })
            
            it('does not modify the navmesh', function(){
                expect(navmesh).toEqual(navmeshBackup)
            })
        })
        
        describe('crossing into other tiles without colliding', function(){
            beforeEach(function(){
                instances.vector = parts.vector(instances)
                instances.plane = parts.plane(instances)
                instances.navmesh = parts.navmesh(instances)
                
                var geometry = {
                    vertices: [
                        [-4, -5, 1],
                        [-2, -5, 1],
                        [-4, -3, 1],
                        [-2, -3, 1],
                        [-4, -1, 1],
                        [-2, -1, 1]
                    ],
                    faces: [
                        [0, 2, 3, 1],
                        [2, 4, 5, 3]
                    ]
                }
                
                navmesh = instances.navmesh.create(geometry)
                navmeshBackup = instances.navmesh.create(geometry)
                
                actor = {
                    location: [-3.50316, -2.66605, 2.0],
                    previousLocation: [-3.12188, -4.08722, 2.0],
                    tile: navmesh[0]
                }
                
                part.collide(actor)
            })
            
            it('does not change location', function(){
                expect(actor.location).toEqual([-3.50316, -2.66605, 2.0])
            })
            
            it('does not change previousLocation', function(){
                expect(actor.previousLocation).toEqual([-3.12188, -4.08722, 2.0])
            })
            
            it('updates tile to the containing tile', function(){
                expect(actor.tile).toBe(navmesh[1])
            })
            
            it('does not modify the navmesh', function(){
                expect(navmesh).toEqual(navmeshBackup)
            })
        })
        
        describe('colliding with a wall without sliding into another tile', function(){
            beforeEach(function(){
                instances.vector = parts.vector(instances)
                instances.plane = parts.plane(instances)
                instances.navmesh = parts.navmesh(instances)
                
                var geometry = {
                    vertices: [
                        [-4, -5, 1],
                        [-2, -5, 1],
                        [-4, -3, 1],
                        [-2, -3, 1],
                        [-4, -1, 1],
                        [-2, -1, 1]
                    ],
                    faces: [
                        [0, 2, 3, 1],
                        [2, 4, 5, 3]
                    ]
                }
                
                navmesh = instances.navmesh.create(geometry)
                navmeshBackup = instances.navmesh.create(geometry)
                
                actor = {
                    location: [-4.25418, -3.7637, 2.0],
                    previousLocation: [-3.12188, -4.08722, 2.0],
                    tile: navmesh[0]
                }
                
                part.collide(actor)
            })            
            
            it('projects location onto the wall', function(){
                expect(actor.location[0]).toBeCloseTo(-4.0)
                expect(actor.location[1]).toBeCloseTo(-3.7637)
                expect(actor.location[2]).toBeCloseTo(2.0)
            })
            
            it('does not change previousLocation', function(){
                expect(actor.previousLocation).toEqual([-3.12188, -4.08722, 2.0])
            })
            
            it('does not change tile', function(){
                expect(actor.tile).toBe(navmesh[0])
            })
            
            it('does not modify the navmesh', function(){
                expect(navmesh).toEqual(navmeshBackup)
            })
        })
        
        describe('colliding with a wall and sliding into another tile', function(){
            beforeEach(function(){
                instances.vector = parts.vector(instances)
                instances.plane = parts.plane(instances)
                instances.navmesh = parts.navmesh(instances)
                
                var geometry = {
                    vertices: [
                        [-4, -5, 1],
                        [-2, -5, 1],
                        [-4, -3, 1],
                        [-2, -3, 1],
                        [-4, -1, 1],
                        [-2, -1, 1]
                    ],
                    faces: [
                        [0, 2, 3, 1],
                        [2, 4, 5, 3]
                    ]
                }
                
                navmesh = instances.navmesh.create(geometry)
                navmeshBackup = instances.navmesh.create(geometry)
                
                actor = {
                    location: [-4.51235, -2.85089, 2.0],
                    previousLocation: [-3.12188, -4.08722, 2.0],
                    tile: navmesh[0]
                }
                
                part.collide(actor)
            })            
            
            it('projects location onto the wall', function(){
                expect(actor.location[0]).toBeCloseTo(-4.0)
                expect(actor.location[1]).toBeCloseTo(-2.85089)
                expect(actor.location[2]).toBeCloseTo(2.0)           
            })
            
            it('does not change previousLocation', function(){
                expect(actor.previousLocation).toEqual([-3.12188, -4.08722, 2.0])
            })
            
            it('changes to the tile moved to', function(){
                expect(actor.tile).toBe(navmesh[1])
            })
            
            it('does not modify the navmesh', function(){
                expect(navmesh).toEqual(navmeshBackup)
            })
        })        
        
        describe('colliding with a wall and sliding into a corner within the same tile', function(){
            beforeEach(function(){
                instances.vector = parts.vector(instances)
                instances.plane = parts.plane(instances)
                instances.navmesh = parts.navmesh(instances)
                
                var geometry = {
                    vertices: [
                        [-4, -5, 1],
                        [-2, -5, 1],
                        [-4, -3, 1],
                        [-2, -3, 1],
                        [-4, -1, 1],
                        [-2, -1, 1]
                    ],
                    faces: [
                        [0, 2, 3, 1],
                        [2, 4, 5, 3]
                    ]
                }
                
                navmesh = instances.navmesh.create(geometry)
                navmeshBackup = instances.navmesh.create(geometry)
                
                actor = {
                    location: [-4.48415, -5.17949, 2.0],
                    previousLocation: [-3.12188, -4.08722, 2.0],
                    tile: navmesh[0]
                }
                
                part.collide(actor)
            })            
            
            it('moves location to the corner', function(){
                expect(actor.location[0]).toBeCloseTo(-4.0)
                expect(actor.location[1]).toBeCloseTo(-5)
                expect(actor.location[2]).toBeCloseTo(2.0)                           
            })
            
            it('does not change previousLocation', function(){
                expect(actor.previousLocation).toEqual([-3.12188, -4.08722, 2.0])
            })
            
            it('does not change tile', function(){
                expect(actor.tile).toBe(navmesh[0])
            })
            
            it('does not modify the navmesh', function(){
                expect(navmesh).toEqual(navmeshBackup)
            })
        })     
        
        describe('colliding with a wall and sliding into a corner within another tile', function(){
            beforeEach(function(){
                instances.vector = parts.vector(instances)
                instances.plane = parts.plane(instances)
                instances.navmesh = parts.navmesh(instances)
                
                var geometry = {
                    vertices: [
                        [-1.27425, -4.90928, 1],
                        [-1.50753, -3.78178, 1],
                        [-1.0021, -3.19859, 1],
                        [-0.14675, -3.56146, 1],
                        [-0.15971, -4.84448, 1]
                    ],
                    faces: [
                        [0, 1, 3, 4],
                        [1, 2, 3]
                    ]
                }
                
                navmesh = instances.navmesh.create(geometry)
                navmeshBackup = instances.navmesh.create(geometry)
                
                actor = {
                    location: [-1.86478, -3.68547, 2],
                    previousLocation: [-0.828, -4.17794, 2.0],
                    tile: navmesh[0]
                }
                
                part.collide(actor)
            })                    
            
            it('moves location to the corner', function(){
                // This looked a bit weird so I checked on Blender and it's
                // actually correct.  It slides past the corner and onto the
                // edge of the neighbor.
                expect(actor.location[0]).toBeCloseTo(-1.4412267019263254)
                expect(actor.location[1]).toBeCloseTo(-3.7052759927119756)
                expect(actor.location[2]).toBeCloseTo(2.0)                           
            })
            
            it('does not change previousLocation', function(){
                expect(actor.previousLocation).toEqual([-0.828, -4.17794, 2.0])
            })
            
            it('updates tile to the containing tile - which could be either', function(){
                expect(actor.tile).toBe(navmesh[1])
            })
            
            it('does not modify the navmesh', function(){
                expect(navmesh).toEqual(navmeshBackup)
            })
        })   
    })
})