describe('timer', function(){
    var timer, _parts
    beforeEach(function(){
        _parts = {
            event: jasmine.createSpy()
        }
        timer = parts.timer(_parts)
    })
    it('does not create an event', function(){
        expect(_parts.event).not.toHaveBeenCalled()
    })
    describe('on calling', function(){
        var instance, eventResult
        beforeEach(function(){
            _parts.event.and.returnValue(eventResult = {})
            instance = timer(5)
        })
        it('creates a new event', function(){
            expect(_parts.event.calls.count()).toEqual(1)
        })
        it('returns the created event', function(){
            expect(instance).toBe(eventResult)
        })
        it('has not defined its own subscribe or raise', function(){
            expect(instance.subscribe).not.toBeDefined()
            expect(instance.raise).not.toBeDefined()
        })
        it('defines the progress through the current tick', function(){
            expect(instance.progress).toBeDefined()
            expect(instance.progress).toEqual(0)
        })
        describe('on calling again', function(){
            var instance2
            beforeEach(function(){
                _parts.event.calls.reset()
                _parts.event.and.returnValue(eventResult = {})
                instance2 = timer(5)
            })
            it('creates a new event', function(){
                expect(_parts.event.calls.count()).toEqual(1)
            })
            it('returns the created event', function(){
                expect(instance2).toBe(eventResult)
            })
        })
        describe('on advancing', function(){
            beforeEach(function(){
                _parts.event.calls.reset()
                instance.raise = jasmine.createSpy()
            })
            describe('by less than a tick', function(){
                beforeEach(function(){
                    instance.advance(0.17)
                })
                it('has not created a new event', function(){
                    expect(_parts.event).not.toHaveBeenCalled()
                })
                it('has not raised itself', function(){
                    expect(instance.raise).not.toHaveBeenCalled()
                })
                it('updates the progress', function(){
                    expect(instance.progress).toBeCloseTo(0.85)
                })
                describe('on advancing less than the remainder of the tick', function(){
                    beforeEach(function(){
                        instance.advance(0.01)
                    })
                    it('has not created a new event', function(){
                        expect(_parts.event).not.toHaveBeenCalled()
                    })
                    it('has not raised itself', function(){
                        expect(instance.raise).not.toHaveBeenCalled()
                    })
                    it('updates the progress', function(){
                        expect(instance.progress).toBeCloseTo(0.9)
                    })                    
                })
                describe('on advancing more than the remainder of the tick', function(){
                    beforeEach(function(){
                        instance.advance(0.04)
                    })
                    it('has not created a new event', function(){
                        expect(_parts.event).not.toHaveBeenCalled()
                    })
                    it('has raised itself', function(){
                        expect(instance.raise.calls.count()).toEqual(1)
                    })
                    it('updates the progress', function(){
                        expect(instance.progress).toBeCloseTo(0.05)
                    })                    
                })
            })
            describe('by a single tick', function(){
                beforeEach(function(){
                    instance.advance(0.21)
                })
                it('has not created a new event', function(){
                    expect(_parts.event).not.toHaveBeenCalled()
                })
                it('has raised itself once', function(){
                    expect(instance.raise.calls.count()).toEqual(1)
                })
                it('updates the progress', function(){
                    expect(instance.progress).toBeCloseTo(0.05)
                })                
            })    
            describe('by multiple ticks', function(){
                beforeEach(function(){
                    instance.advance(0.62)
                })
                it('has not created a new event', function(){
                    expect(_parts.event).not.toHaveBeenCalled()
                })
                it('has raised itself three times', function(){
                    expect(instance.raise.calls.count()).toEqual(3)
                })
                it('updates the progress', function(){
                    expect(instance.progress).toBeCloseTo(0.1)
                })                
            })               
        })
    })
})