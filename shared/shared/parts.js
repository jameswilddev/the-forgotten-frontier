/* A collection of all dependency injectable parts in the application.
 To add a new part, add a function to this taking an object containing the
 instantiated parts as its argument and returning the instantiated part.
 Order of instantiation is not guaranteed. */
parts = {
    window: function(){
        return window
    }
}

/* Creates a container of instances from a parts container. */
build = function(parts){
    var instances = {}
    for(var part in parts)
        instances[part] = parts[part](instances)
    return instances
}