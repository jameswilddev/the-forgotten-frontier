describe('event', function(){
   var event, instance
   beforeEach(function(){
       event = parts.event()
       instance = event()
   })
   it('returns a new instance on each call', function(){
       expect(instance).not.toBe(event())
   })
   it('does nothing on raising with no subscribers', function(){
       instance.raise()
   })
   describe('after subscribing', function(){
       var callbackA, resultA
       beforeEach(function(){
           callbackA = jasmine.createSpy()
           resultA = instance.subscribe(callbackA)
       })
       it('does not execute the callback', function(){
           expect(callbackA).not.toHaveBeenCalled()
       })
       describe('on raising', function(){
           beforeEach(function(){
               instance.raise()
           })
           it('executes the callback with the unsubscribe function as the argument', function(){
               expect(callbackA.calls.count()).toEqual(1)
               expect(callbackA.calls.argsFor(0)[0]).toBe(resultA)
           })
           describe('on raising again', function(){
               beforeEach(function(){
                   callbackA.calls.reset()
                   instance.raise()
               })
               it('executes the callback with the unsubscribe function as the argument', function(){
                   expect(callbackA.calls.count()).toEqual(1)
                   expect(callbackA.calls.argsFor(0)[0]).toBe(resultA)
               })
           })
       })
       describe('on creating another event and raising that event', function(){
           beforeEach(function(){
               event().raise()
           })
           it('does not execute the callback', function(){
               expect(callbackA).not.toHaveBeenCalled()
           })
       })
       describe('on unsubscribing', function(){
           beforeEach(function(){
               resultA()
           })
           it('does not execute the callback', function(){
              expect(callbackA).not.toHaveBeenCalled() 
           })
           describe('on raising', function(){
               beforeEach(function(){
                   instance.raise()
               })
               it('does not execute the callback', function(){
                  expect(callbackA).not.toHaveBeenCalled() 
               })
           })
       })
       describe('on adding a second subscriber', function(){
           var callbackB, resultB
           beforeEach(function(){
               callbackB = jasmine.createSpy()
               resultB = instance.subscribe(callbackB)
           })
           it('does not execute either callback', function(){
              expect(callbackA).not.toHaveBeenCalled() 
              expect(callbackB).not.toHaveBeenCalled() 
           })    
           describe('on raising', function(){
               beforeEach(function(){
                   instance.raise()
               })
               it('executes the callbacks with their unsubscribe functions as the argument', function(){
                   expect(callbackA.calls.count()).toEqual(1)
                   expect(callbackA.calls.argsFor(0)[0]).toBe(resultA)
                   expect(callbackB.calls.count()).toEqual(1)
                   expect(callbackB.calls.argsFor(0)[0]).toBe(resultB)                   
               })
           })
           describe('on unsubscribing the first callback', function(){
               beforeEach(function(){
                   resultA()
               })
               it('does not execute either callback', function(){
                  expect(callbackA).not.toHaveBeenCalled() 
                  expect(callbackB).not.toHaveBeenCalled() 
               })    
               describe('on raising', function(){
                   beforeEach(function(){
                       instance.raise()
                   })
                   it('does not execute the first callback', function(){
                       expect(callbackA).not.toHaveBeenCalled()
                   })
                   it('executes the second callback with its unsubscribe function as the argument', function(){
                       expect(callbackB.calls.count()).toEqual(1)
                       expect(callbackB.calls.argsFor(0)[0]).toBe(resultB)                   
                   })
               })
           })
           describe('on unsubscribing the second callback', function(){
               beforeEach(function(){
                   resultB()
               })
               it('does not execute either callback', function(){
                  expect(callbackA).not.toHaveBeenCalled() 
                  expect(callbackB).not.toHaveBeenCalled() 
               })    
               describe('on raising', function(){
                   beforeEach(function(){
                       instance.raise()
                   })
                   it('does not execute the second callback', function(){
                       expect(callbackB).not.toHaveBeenCalled()
                   })
                   it('executes the first callback with its unsubscribe function as the argument', function(){
                       expect(callbackA.calls.count()).toEqual(1)
                       expect(callbackA.calls.argsFor(0)[0]).toBe(resultA)                   
                   })
               })
           })           
       })
   })
})