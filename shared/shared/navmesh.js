
parts.navmesh = function(instances) {
    var part
    return part = {
        /* Converts a given Lightwave/OBJ mesh object into a navmesh.
           This converts every face into a plane with an array of edges. 
           (objects  containing a plane facing out of the the navmesh tile and 
           optionally the reference to a neighbor navmesh tile) */        
        create: function(obj){
            var output = []
            for(var index = 0; index < obj.faces.length; index++) {
                var tile = {}
                var vertices = []
                for(var vertex = 0; vertex < obj.faces[index].length; vertex++)
                    vertices.push(obj.vertices[obj.faces[index][vertex]])
                instances.plane.create(vertices, tile)
                tile.edges = []
                for(var vertex = 0; vertex < obj.faces[index].length; vertex++){
                    var edge = {}
                    var current = obj.vertices[obj.faces[index][vertex]]
                    var temp = []
                    instances.vector.add(current, tile.normal, temp)
                    instances.plane.create([current, obj.vertices[obj.faces[index][(vertex + 1) % obj.faces[index].length]], temp], edge)
                    edge.neighbor = null
                    tile.edges.push(edge)
                }
                output.push(tile)
            }
            
            //Find neighbors that have matching 
            for(var face = 0; face < obj.faces.length; face++) {
                for(var vertex = 0; vertex < obj.faces[face].length; vertex++) {
                    for(var otherFace = 0; otherFace < obj.faces.length; otherFace++) {
                        for(var otherVertex = 0; otherVertex < obj.faces[otherFace].length; otherVertex++) {
                            if(obj.faces[face][vertex] != obj.faces[otherFace][(otherVertex + 1) % obj.faces[otherFace].length]) continue
                            if(obj.faces[otherFace][otherVertex] != obj.faces[face][(vertex + 1) % obj.faces[face].length]) continue
                            output[face].edges[vertex].neighbor = output[otherFace]
                        }
                    }                
                }
            }
            return output
        },
        
        /* Given a navmesh tile and the start and end of a ray, finds the edge
        the ray crosses, if any. */
        getCrossedEdge: function(tile, from, to) {
            var closestEdge, closestAlong = Infinity
            for(var edge = 0; edge < tile.edges.length; edge++) {
                var start = instances.plane.distance(tile.edges[edge], from)
                if(start > 0.001) continue
                var end = instances.plane.distance(tile.edges[edge], to)
                if(end < -0.001) continue
                var along = (-start) / (end  - start)
                if(along >= closestAlong) continue
                closestEdge = tile.edges[edge]
                closestAlong = along
            }
            return closestEdge
        },
        
        /* Given a navmesh tile, the start and end of a ray, a number of
           iterations to perform, and a callback to execute on encountering an
           edge with no neighbor, traverses the navmesh to try and determine
           which tile the ray ends in.
           - If the end is in the current tile, the current tile is returned.
           - If the end is over an edge, but we have no iterations left, the
             tile we would otherwise enter is returned.
           - If the end is over an edge, and that edge has a neighbor, and we
             have remaining iterations, this function recursively calls itself 
             and returns the end result.
           - If the end is over an edge, but that edge has no neighbor, the
             callback is executed with the current tile and number of iterations 
             remaining.  The return value of the callback is returned. */
        traverse: function(tile, from, to, iterations) {
            var crossed = part.getCrossedEdge(tile, from, to)
            if(crossed && crossed.neighbor) {
                if(iterations) 
                    return part.traverse(crossed.neighbor, from, to, iterations - 1)
                return crossed.neighbor
            }
            return tile
        },
        
        /* Given a navmesh tile, a location and an output vector, constrains the
        location to be within the edges of the tile and writes it to the output.
        Note that this does not project it onto the surface of the tile; 
        altitude is preserved. */
        constrainToEdges: function(tile, location, output) {
            // This might be implemented better in future.
            // At present, it's possible that the point is over multiple edges
            // so I have to find all the ones it was over and repeatedly project
            // it into those planes until it settles into the right spot.
            var crossed = []
            for(var edge = 0; edge < tile.edges.length; edge++)
                if(instances.plane.before(tile.edges[edge], location))
                    crossed.push(tile.edges[edge])
            if(!crossed.length) {
                instances.vector.clone(location, output)
                return
            }
            if(crossed.length == 1) {
                instances.plane.project(crossed[0], location, output)
                return
            }
            instances.vector.clone(location, output)
            var iterations = 2
            while(iterations--) {
                for(var edge = 0; edge < crossed.length; edge++)
                    instances.plane.project(crossed[edge], output, output)
            }
        },
        
        /* Given a navmesh tile, a location and an output vector, constrains the
        location to be within the edges of the tile and on the surface of the
        tile and writes it to the output. */
        constrainToTile: function(tile, location, output) {
            part.constrainToEdges(tile, location, output)
            instances.plane.project(tile, output, output)
        },
        
        /* Given a navmesh tile and a location, returns the distance between
        that tile and that location. */
        tileDistance: function(tile, location) {
            var temp = []
            part.constrainToTile(tile, location, temp)
            return instances.vector.distance(temp, location)
        },
        
        /* Given a navmesh and a location, returns the tile which is closest
        to that point. */
        closestTile: function(navmesh, location) {
            var closest = Infinity, closestTile
            for(var tile = 0; tile < navmesh.length; tile++) {
                var distance = part.tileDistance(navmesh[tile], location)
                if(distance > closest) continue
                closest = distance
                closestTile = navmesh[tile]
            }
            return closestTile
        }
    }
}