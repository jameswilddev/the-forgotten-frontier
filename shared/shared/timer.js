// Call with a number of events per second to get a new event.
// On this, call advance with the number of seconds elapsed and it will raise 
// itself like a normal event.  Subscribe/raise work as normal.
parts.timer = function(parts){
    return function(ticksPerSecond){
        var output = parts.event()
        output.progress = 0.0
        output.advance = function(seconds) {
            output.progress += seconds * ticksPerSecond
            while(output.progress >= 1.0) {
                output.raise()
                output.progress -= 1.0
            }
        }
        return output
    }
}