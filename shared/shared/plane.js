/* Tools for working with planes.  A plane is an object containing a normal vector and
   a distance float.  { normal: [1, 0, 0], distance: 30 ) is a plane pointing to X+ at
   30, 0, 0.
   - create takes an array of points on a plane, and writes the normal/distance to the
     second argument.
   - distance returns the distance between a given plane and a given vector.  When 
     behind the plane, the distance will be negative.
   - before returns true when the given plane is behind the given vector.
   - behind returns true when the given plane is in front of the given vector.
   - project projects onto the given plane the given vector, writing the vector to the
     last argument. 
   - intersect takes the start and end of a ray and returns where between the two it
     intersects the plane.  (from 0 to 1)  Note that this may be beyond 0 or 1. */
parts.plane = function(instances){
    var part
    return part = {
        create: function(vertices, output) {
            var diffA = [], diffB = []
            instances.vector.subtract(vertices[2], vertices[0], diffA)
            instances.vector.subtract(vertices[1], vertices[0], diffB)
            output.normal = []
            instances.vector.cross(diffA, diffB, output.normal)
            instances.vector.normalize(output.normal, output.normal)
            output.distance = instances.vector.dot(output.normal, vertices[0])
        },
        distance: function(plane, input) {
            return instances.vector.dot(plane.normal, input) - plane.distance
        },
        before: function(plane, input) {
            return part.distance(plane, input) > 0
        },
        behind: function(plane, input) {
            return part.distance(plane, input) < 0
        },
        project: function(plane, input, output) {
            var temp = []
            instances.vector.multiply([part.distance(plane, input)], plane.normal, temp)
            instances.vector.subtract(input, temp, output)
            return output
        },
        intersect: function(plane, start, end) {
            var before = part.distance(plane, start)
            var after = part.distance(plane, end)
            return before / (before - after)
        }
    }
}