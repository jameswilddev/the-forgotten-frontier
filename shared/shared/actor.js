
/* An actor is an object containing:
   tile - a reference to the navmesh tile containing the actor.
   location - the current location of the actor.
   previousLocation - the location of the actor the previous screen. */
parts.actor = function(instances) {
    return {
        /* Creates a new actor from the navmesh containing it and the location 
           it should be created at. */
        create: function(navmesh, location) {
            var output = {
                tile: instances.navmesh.closestTile(navmesh, location),
                location: [],
                previousLocation: []
            }           

            instances.navmesh.constrainToTile(output.tile, location, output.location)
            instances.vector.clone(output.location, output.previousLocation)
            return output
        },
        
        /* Given an actor where the location has been changed, traces from
           previousLocation using the current tile, collides against tile edges
           without neighbors using a sliding collision algorithm and crosses
           over tile edges which do have neighbors.  This does not constrain to
           the tile surfaces, only their edges. */
        collide: function(actor) {
            var iterations = 3
            var temp = []
            instances.vector.clone(actor.previousLocation, temp)
            while(iterations--) {
                actor.tile = instances.navmesh.traverse(actor.tile, temp, actor.location, 5)
                var exiting = instances.navmesh.getCrossedEdge(actor.tile, temp, actor.location)
                if(exiting && !exiting.neighbor) {
                    instances.vector.interpolate(temp, actor.location, [instances.plane.intersect(exiting, temp, actor.location)], temp)
                    instances.plane.project(exiting, actor.location, actor.location)
                    
                    // Push the points back inside the navmesh tile.  This prevents repeat collisions against the same edge.
                    var temp2 = []
                    instances.vector.multiply(exiting.normal, [-0.002], temp2)
                    instances.vector.add(actor.location, temp2, actor.location)
                    instances.vector.add(temp, temp2, temp)
                }
            }
            
            // As a last-ditch effort, if we run out of iterations to resolve the collision, fall back to 
            instances.navmesh.constrainToEdges(actor.tile, actor.location, actor.location)
        }
    }
}