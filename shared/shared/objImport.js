/* Imports the vertices of a given Lightwave/OBJ file as an object containing
   vertices as an array of vectors (themselves arrays of floats) and faces
   as an array of arrays of indices into the vertex array. */
parts.objImport = function(){
    return function(file) {
        var output = {
            vertices: [],
            faces: []
        }
        
        var lines = file.split('\n')
        
        for(var line = 0; line < lines.length; line++) {
            var tokens = lines[line].split(/\s+/)
            switch(tokens[0].toLowerCase()) {
                case 'v':
                    var vertex = []
                    for(var index = 1; index < tokens.length; index++)
                        vertex.push(parseFloat(tokens[index]))
                    output.vertices.push(vertex)
                    break
                    
                case 'f':
                    var face = []
                    for(var index = 1; index < tokens.length; index++)
                        face.push(parseInt(tokens[index]) - 1)
                    output.faces.push(face)
                    break
            }
        }
        
        return output
    }
}