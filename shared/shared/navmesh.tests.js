describe('navmesh', function(){
    var part, instances
    beforeEach(function(){
        instances = {}
        instances.plane = parts.plane(instances)
        instances.vector = parts.vector(instances)
        part = parts.navmesh(instances)
    })
    describe('create', function(){
        it('builds an object for each face', function(){
            var navmesh = part.create({
                vertices: [
                    [-1, -1, 0],
                    [0, 1, 0],
                    [3, 3, 1],
                    [3, 5, 1],
                    [5, 5, 1],
                    [1, -1, 0],
                    [5, 3, 1]
                ],
                faces: [
                    [0, 1, 5],
                    [3, 4, 2, 6]
                ]
            })
            
            expect(navmesh.length).toEqual(2)
        })
        it('generates a plane for each face', function(){
            var navmesh = part.create({
                vertices: [
                    [-1, -1, 3],
                    [0, 1, 3],
                    [3, 3, 2],
                    [3, 5, 2],
                    [5, 5, 1],
                    [1, -1, 3],
                    [5, 3, 1]
                ],
                faces: [
                    [0, 1, 5],
                    [3, 4, 2, 6]
                ]
            })
            
            expect(navmesh[0].normal.length).toEqual(3)
            expect(navmesh[0].normal[0]).toBeCloseTo(0)
            expect(navmesh[0].normal[1]).toBeCloseTo(0)
            expect(navmesh[0].normal[2]).toBeCloseTo(1)
            expect(navmesh[0].distance).toBeCloseTo(3)
    
            expect(navmesh[1].normal.length).toEqual(3)
            expect(navmesh[1].normal[0]).toBeCloseTo(0.44721)
            expect(navmesh[1].normal[1]).toBeCloseTo(0)
            expect(navmesh[1].normal[2]).toBeCloseTo(0.89443)
            expect(navmesh[1].distance).toBeCloseTo(3.13)
        })
        it('builds a plane for each edge of each face', function(){
            var navmesh = part.create({
                vertices: [
                    [-1, -1, 3],
                    [0, 1, 3],
                    [3, 3, 2],
                    [3, 5, 2],
                    [5, 5, 1],
                    [1, -1, 3],
                    [5, 3, 1]
                ],
                faces: [
                    [0, 1, 5],
                    [3, 4, 6, 2]
                ]
            })
            
            expect(navmesh[0].edges.length).toEqual(3)
            expect(navmesh[0].edges[0].normal.length).toEqual(3)
            expect(navmesh[0].edges[0].normal[0]).toBeCloseTo(-0.89443)
            expect(navmesh[0].edges[0].normal[1]).toBeCloseTo(0.44721)
            expect(navmesh[0].edges[0].normal[2]).toBeCloseTo(0)     
            expect(navmesh[0].edges[1].normal.length).toEqual(3)
            expect(navmesh[0].edges[1].normal[0]).toBeCloseTo(0.89443)
            expect(navmesh[0].edges[1].normal[1]).toBeCloseTo(0.44721)
            expect(navmesh[0].edges[1].normal[2]).toBeCloseTo(0)                
            expect(navmesh[0].edges[2].normal.length).toEqual(3)
            expect(navmesh[0].edges[2].normal[0]).toBeCloseTo(0)
            expect(navmesh[0].edges[2].normal[1]).toBeCloseTo(-1)
            expect(navmesh[0].edges[2].normal[2]).toBeCloseTo(0)        
            
            expect(navmesh[1].edges.length).toEqual(4)
            expect(navmesh[1].edges[0].normal.length).toEqual(3)
            expect(navmesh[1].edges[0].normal[0]).toBeCloseTo(0.0)
            expect(navmesh[1].edges[0].normal[1]).toBeCloseTo(1.0)
            expect(navmesh[1].edges[0].normal[2]).toBeCloseTo(0)     
            expect(navmesh[1].edges[1].normal.length).toEqual(3)        
            expect(navmesh[1].edges[1].normal[0]).toBeCloseTo(0.89443)
            expect(navmesh[1].edges[1].normal[1]).toBeCloseTo(0)
            expect(navmesh[1].edges[1].normal[2]).toBeCloseTo(-0.44721)     
            expect(navmesh[1].edges[2].normal.length).toEqual(3)
            expect(navmesh[1].edges[2].normal[0]).toBeCloseTo(0.0)
            expect(navmesh[1].edges[2].normal[1]).toBeCloseTo(-1.0)
            expect(navmesh[1].edges[2].normal[2]).toBeCloseTo(0)  
            expect(navmesh[1].edges[3].normal.length).toEqual(3)        
            expect(navmesh[1].edges[3].normal[0]).toBeCloseTo(-0.89443)
            expect(navmesh[1].edges[3].normal[1]).toBeCloseTo(0)
            expect(navmesh[1].edges[3].normal[2]).toBeCloseTo(0.44721)     
        })
        it('leaves the neighbor null where there is no neighboring face', function(){
            var navmesh = part.create({
                vertices: [
                    [-2, -4, 6],
                    [-2, -2, 3],
                    [-0.5, -0.5, 8],
                    [0, -2, 6],
                    [0, -4, 3]
                ],
                faces: [
                    [0, 1, 3, 4],
                    [1, 2, 3]
                ]
            })     
            
            expect(navmesh[0].edges[3].neighbor).toBeFalsy()
        })
        it('leaves the neighbor null when only one vertex aligns', function(){
            var navmesh = part.create({
                vertices: [
                    [-2, -4, 6],
                    [-2, -2, 3],
                    [-0.5, -0.5, 8],
                    [0, -2, 6],
                    [0, -4, 3]
                ],
                faces: [
                    [0, 1, 3, 4],
                    [1, 2, 3]
                ]
            })     
            
            expect(navmesh[0].edges[0].neighbor).toBeFalsy()        
            expect(navmesh[0].edges[2].neighbor).toBeFalsy()        
            expect(navmesh[1].edges[0].neighbor).toBeFalsy()        
            expect(navmesh[1].edges[1].neighbor).toBeFalsy()        
        })    
        it('sets the neighbor to a reference to the neighboring face when two vertices align', function(){
            var navmesh = part.create({
                vertices: [
                    [-2, -4, 6],
                    [-2, -2, 3],
                    [-0.5, -0.5, 8],
                    [0, -2, 6],
                    [0, -4, 3]
                ],
                faces: [
                    [0, 1, 3, 4],
                    [1, 2, 3]
                ]
            })     
            
            expect(navmesh[0].edges[1].neighbor).toBe(navmesh[1])        
            expect(navmesh[1].edges[2].neighbor).toBe(navmesh[0])
        })
        
        it('does not modify the inputs', function(){
            var obj = {
                vertices: [
                    [-2, -4, 6],
                    [-2, -2, 3],
                    [-0.5, -0.5, 8],
                    [0, -2, 6],
                    [0, -4, 3]
                ],
                faces: [
                    [0, 1, 3, 4],
                    [1, 2, 3]
                ]
            }
            
            part.create(obj)             
            
            expect(obj).toEqual({
                vertices: [
                    [-2, -4, 6],
                    [-2, -2, 3],
                    [-0.5, -0.5, 8],
                    [0, -2, 6],
                    [0, -4, 3]
                ],
                faces: [
                    [0, 1, 3, 4],
                    [1, 2, 3]
                ]
            })
        })
    })
    
    describe('getCrossedEdge', function(){
        var tile
        beforeEach(function(){
            tile = part.create({
                vertices: [
                    [3.15274, 1.7494, 1.0],
                    [1.43437, 3.17661, 1.0],
                    [4.06444, 3.54892, 1.0],
                    [4.9809, 1.57756, 1.0]
                ],
                faces: [
                    [0, 1, 2, 3]
                ]
            })[0]
        })
        
        it('returns null when not leaving the tile', function(){
            var start = [2.53138, 2.90765, 1.0]
            var end = [4.19717, 2.1186, 1.0]
            
            expect(part.getCrossedEdge(tile, start, end)).toBeFalsy()
        })
        
        it('returns null when entering the tile without leaving', function(){
            var start = [3.86197, 0.39301, 1.0]
            var end = [4.19717, 2.1186, 1.0]
            
            expect(part.getCrossedEdge(tile, start, end)).toBeFalsy()
        })
        
        it('returns the edge crossed when leaving the tile', function(){
            var start = [4.19717, 2.1186, 1.0]
            var end = [3.43227, 1.60731, 1.0]
            
            expect(part.getCrossedEdge(tile, start, end)).toBe(tile.edges[3])            
        })
        
        it('returns the edge crossed when leaving the tile even if it ends closer to another tile', function(){
            var start = [4.19717, 2.1186, 1.0]
            var end = [2.64828, 1.65101, 1.0]
            
            expect(part.getCrossedEdge(tile, start, end)).toBe(tile.edges[0])               
        })        
        
        it('returns the edge crossed while leaving the tile when both entering and leaving', function(){
            var start = [2.82839, 0.58072, 1.0]
            var end = [4.54874, 4.63052, 1.0]
            
            expect(part.getCrossedEdge(tile, start, end)).toBe(tile.edges[2])               
        })
        
        it('has a small amount of tolerance to allow for floating point inaccuracy', function(){
            var start = [3.02939, 1.85133, 1.0]
            var end = [2.60042, 2.20822, 1.0]
            
            expect(part.getCrossedEdge(tile, start, end)).toBe(tile.edges[0])               
        })
        
        it('does not modify its inputs', function(){
            var start = [4, 6, 8]
            var end = [6, 2, 7]
            
            part.getCrossedEdge(tile, start, end)
            
            expect(tile).toEqual(part.create({
                vertices: [
                    [3.15274, 1.7494, 1.0],
                    [1.43437, 3.17661, 1.0],
                    [4.06444, 3.54892, 1.0],
                    [4.9809, 1.57756, 1.0]
                ],
                faces: [
                    [0, 1, 2, 3]
                ]
            })[0])
            
            expect(start).toEqual([4, 6, 8])
            expect(end).toEqual([6, 2, 7])
        })
    })
    
    describe('traverse', function(){
        var func, navmesh, navmeshBackup, start, end, result
        beforeEach(function(){
            func = part.traverse
            part.traverse = jasmine.createSpy()
            navmesh = part.create({
                vertices: [
                    [1, -1, 0],
                    [-1, -1, 0],
                    [-1, 1, 0],
                    [1, 1, 0],
                    [-2, -1, -1],
                    [-3, 1, -2],
                    [-1, 3, 0],
                    [-3, 3, -2],
                    [-4, 1, -2],
                    [-4, 3, -2]
                ],
                faces: [
                    [0, 1, 2, 3],
                    [1, 4, 5, 2],
                    [5, 8, 9, 7],
                    [2, 5, 7, 6]
                ]
            })
            navmeshBackup = part.create({
                vertices: [
                    [1, -1, 0],
                    [-1, -1, 0],
                    [-1, 1, 0],
                    [1, 1, 0],
                    [-2, -1, -1],
                    [-3, 1, -2],
                    [-1, 3, 0],
                    [-3, 3, -2],
                    [-4, 1, -2],
                    [-4, 3, -2]
                ],
                faces: [
                    [0, 1, 2, 3],
                    [1, 4, 5, 2],
                    [5, 8, 9, 7],
                    [2, 5, 7, 6]
                ]
            })            
        })
        
        describe('when the end is within the current tile', function(){
            beforeEach(function(){
                start = [0.62628, -0.2856, 3.1]
                end = [-0.71912, 0.43194, -8.4]
                result = func(navmesh[0], start, end)
            })
            
            it('returns the current tile', function(){
                expect(result).toBe(navmesh[0])
            })
            
            it('does not recurse', function(){
                expect(part.traverse).not.toHaveBeenCalled()
            })
            
            it('does not modify its inputs', function(){
                expect(navmesh).toEqual(navmeshBackup)
                expect(start).toEqual([0.62628, -0.2856, 3.1])
                expect(end).toEqual([-0.71912, 0.43194, -8.4])
            })
        })
        
        describe('the end is over an edge', function(){
            describe('the edge has a neighbor', function(){
                describe('iterations remain', function(){
                    beforeEach(function(){
                        start = [0.62628, -0.2856, 3.1]
                        end = [-2.26782, -0.04774, -8.4]
                        part.traverse.and.callFake(function(_tile, _start, _end, _iterations){
                            if(_tile != navmesh[1]
                                || _start != start
                                || _end != end
                                || _iterations != 0)
                                return
                            return 'Test Recursed'
                        })
                        result = func(navmesh[0], start, end, 1)
                    })                    
                    
                    it('calls itself recursively', function(){
                        expect(part.traverse).toHaveBeenCalled()
                    })
                    
                    it('returns its recursed result', function(){
                        expect(result).toBe('Test Recursed')
                    })
                    
                    it('does not modify its inputs', function(){
                        expect(navmesh).toEqual(navmeshBackup)
                        expect(start).toEqual([0.62628, -0.2856, 3.1])
                        expect(end).toEqual([-2.26782, -0.04774, -8.4])
                    })                    
                })
                
                describe('no iterations remain', function(){
                    beforeEach(function(){
                        start = [0.62628, -0.2856, 3.1]
                        end = [-2.26782, -0.04774, -8.4]
                        result = func(navmesh[0], start, end, 0)                        
                    })
                    
                    it('returns the neighbor', function(){
                        expect(result).toBe(navmesh[1])
                    })
                    
                    it('does not recurse', function(){
                        expect(part.traverse).not.toHaveBeenCalled()
                    })
                    
                    it('does not modify its inputs', function(){
                        expect(navmesh).toEqual(navmeshBackup)
                        expect(start).toEqual([0.62628, -0.2856, 3.1])
                        expect(end).toEqual([-2.26782, -0.04774, -8.4])
                    })
                })
            })
            
            describe('the edge does not have a neighbor', function(){
                    beforeEach(function(){
                        start = [0.62628, -0.2856, 3.1]
                        end = [0.04738, 1.12798, 18.4]
                        result = func(navmesh[0], start, end, 0)                        
                    })                
                
                it('returns the current tile', function(){
                    expect(result).toBe(navmesh[0])
                })
                
                it('does not modify its inputs', function(){
                    expect(navmesh).toEqual(navmeshBackup)
                    expect(start).toEqual([0.62628, -0.2856, 3.1])
                    expect(end).toEqual([0.04738, 1.12798, 18.4])                        
                })                
            })
        })
    })
    
    describe('constrainToEdges', function(){
        var geometry = {
            vertices: [
                [-1.76879, -0.13216, 1.21215],
                [-0.67803, 0.72273, 2.14684],
                [0.18378, -0.75249, 1.94261],
                [-0.81961, -1.70416, 1.01036]
            ],
            faces: [
                [0, 1, 2, 3]
            ]
        }
        var tile, tileBackup, input, output
        beforeEach(function(){
            tile = part.create(geometry)[0]
            tileBackup = part.create(geometry)[0]
        })
        
        describe('above the surface', function(){
            describe('not over an edge', function(){
                beforeEach(function(){
                    input = [-0.65197, -0.85034, 2.10156]
                })
                
                it('outputs the same point', function(){
                    part.constrainToEdges(tile, input, output = [])
                    expect(output.length).toEqual(3)
                    expect(output[0]).toBeCloseTo(-0.65197)
                    expect(output[1]).toBeCloseTo(-0.85034)
                    expect(output[2]).toBeCloseTo(2.10156)
                })
                
                it('supports having the input as the output', function(){
                    part.constrainToEdges(tile, input, input)
                    expect(input.length).toEqual(3)
                    expect(input[0]).toBeCloseTo(-0.65197)
                    expect(input[1]).toBeCloseTo(-0.85034)
                    expect(input[2]).toBeCloseTo(2.10156)
                })
                
                it('does not modify its inputs', function(){
                    part.constrainToEdges(tile, input, [])
                    expect(tile).toEqual(tileBackup)
                    expect(input).toEqual([-0.65197, -0.85034, 2.10156])
                })
            })
            
            describe('over one edge', function(){
                beforeEach(function(){
                    input = [-2.27156, 0.58128, 1.89766]
                })                
                
                it('projects the point onto the edge', function(){
                    part.constrainToEdges(tile, input, output = [])
                    expect(output.length).toEqual(3)
                    expect(output[0]).toBeCloseTo(-1.71714)
                    expect(output[1]).toBeCloseTo(-0.10728)
                    expect(output[2]).toBeCloseTo(1.88045)
                })
                
                it('supports having the input as the output', function(){
                    part.constrainToEdges(tile, input, input)
                    expect(input.length).toEqual(3)
                    expect(input[0]).toBeCloseTo(-1.71714)
                    expect(input[1]).toBeCloseTo(-0.10728)
                    expect(input[2]).toBeCloseTo(1.88045)
                })                
                
                it('does not modify its inputs', function(){
                    part.constrainToEdges(tile, input, [])
                    expect(tile).toEqual(tileBackup)
                    expect(input).toEqual([-2.27156, 0.58128, 1.89766])
                })            
            })
            
            describe('over two edges', function(){
                beforeEach(function(){
                    input = [-2.92829, 0.06657, 1.33491]
                })                    
                
                it('projects the point onto the border between the edges', function(){
                    part.constrainToEdges(tile, input, output = [])
                    expect(output.length).toEqual(3)
                    expect(output[0]).toBeCloseTo(-1.99128)
                    expect(output[1]).toBeCloseTo(-0.32213)
                    expect(output[2]).toBeCloseTo(1.64555)
                })
                
                it('supports having the input as the output', function(){
                    part.constrainToEdges(tile, input, input)
                    expect(input.length).toEqual(3)
                    expect(input[0]).toBeCloseTo(-1.99128)
                    expect(input[1]).toBeCloseTo(-0.32213)
                    expect(input[2]).toBeCloseTo(1.64555)
                })                
                
                it('does not modify its inputs', function(){
                    part.constrainToEdges(tile, input, [])
                    expect(tile).toEqual(tileBackup)
                    expect(input).toEqual([-2.92829, 0.06657, 1.33491])
                })            
            })        
        })
        
        describe('below the surface', function(){
            describe('not over an edge', function(){
                beforeEach(function(){
                    input = [-0.52784, -0.21329, 1.207]
                })
                
                it('outputs the same point', function(){
                    part.constrainToEdges(tile, input, output = [])
                    expect(output.length).toEqual(3)
                    expect(output[0]).toBeCloseTo(-0.52784)
                    expect(output[1]).toBeCloseTo(-0.21329)
                    expect(output[2]).toBeCloseTo(1.207)
                })
                
                it('supports having the input as the output', function(){
                    part.constrainToEdges(tile, input, input)
                    expect(input.length).toEqual(3)
                    expect(input[0]).toBeCloseTo(-0.52784)
                    expect(input[1]).toBeCloseTo(-0.21329)
                    expect(input[2]).toBeCloseTo(1.207)
                })
                
                it('does not modify its inputs', function(){
                    part.constrainToEdges(tile, input, [])
                    expect(tile).toEqual(tileBackup)
                    expect(input).toEqual([-0.52784, -0.21329, 1.207])
                })
            })
            
            describe('over one edge', function(){
                beforeEach(function(){
                    input = [-1.89376, 0.90387, 1.16174]
                })                
                
                it('projects the point onto the edge', function(){
                    part.constrainToEdges(tile, input, output = [])
                    expect(output.length).toEqual(3)
                    expect(output[0]).toBeCloseTo(-1.33935)
                    expect(output[1]).toBeCloseTo(0.21531)
                    expect(output[2]).toBeCloseTo(1.14453)
                })
                
                it('supports having the input as the output', function(){
                    part.constrainToEdges(tile, input, input)
                    expect(input.length).toEqual(3)
                    expect(input[0]).toBeCloseTo(-1.33935)
                    expect(input[1]).toBeCloseTo(0.21531)
                    expect(input[2]).toBeCloseTo(1.14453)
                })                
                
                it('does not modify its inputs', function(){
                    part.constrainToEdges(tile, input, [])
                    expect(tile).toEqual(tileBackup)
                    expect(input).toEqual([-1.89376, 0.90387, 1.16174])
                })            
            })
            
            describe('over two edges', function(){
                beforeEach(function(){
                    input = [-2.66203, 0.30174, 0.5034]
                })                    
                
                it('projects the point onto the border between the edges', function(){
                    part.constrainToEdges(tile, input, output = [])
                    expect(output.length).toEqual(3)
                    expect(output[0]).toBeCloseTo(-1.61348)
                    expect(output[1]).toBeCloseTo(0.00046)
                    expect(output[2]).toBeCloseTo(0.90962)
                })
                
                it('supports having the input as the output', function(){
                    part.constrainToEdges(tile, input, input)
                    expect(input.length).toEqual(3)
                    expect(input[0]).toBeCloseTo(-1.61348)
                    expect(input[1]).toBeCloseTo(0.00046)
                    expect(input[2]).toBeCloseTo(0.90962)
                })                
                
                it('does not modify its inputs', function(){
                    part.constrainToEdges(tile, input, [])
                    expect(tile).toEqual(tileBackup)
                    expect(input).toEqual([-2.66203, 0.30174, 0.5034])
                })            
            })        
        })      
    })
    
    describe('constrainToTile', function(){
        var geometry = {
            vertices: [
                [-1.76879, -0.13216, 1.21215],
                [-0.67803, 0.72273, 2.14684],
                [0.18378, -0.75249, 1.94261],
                [-0.81961, -1.70416, 1.01036]
            ],
            faces: [
                [0, 1, 2, 3]
            ]
        }
        var tile, tileBackup, input, output
        beforeEach(function(){
            tile = part.create(geometry)[0]
            tileBackup = part.create(geometry)[0]
        })
        
        describe('above the surface', function(){
            describe('not over an edge', function(){
                beforeEach(function(){
                    input = [-0.65197, -0.85034, 2.10156]
                })
                
                it('projects the point onto the surface', function(){
                    part.constrainToTile(tile, input, output = [])
                    expect(output.length).toEqual(3)
                    expect(output[0]).toBeCloseTo(-0.42948)
                    expect(output[1]).toBeCloseTo(-0.66037)
                    expect(output[2]).toBeCloseTo(1.66817)
                })
                
                it('supports having the input as the output', function(){
                    part.constrainToTile(tile, input, input)
                    expect(input.length).toEqual(3)
                    expect(input[0]).toBeCloseTo(-0.42948)
                    expect(input[1]).toBeCloseTo(-0.66037)
                    expect(input[2]).toBeCloseTo(1.66817)
                })
                
                it('does not modify its inputs', function(){
                    part.constrainToTile(tile, input, [])
                    expect(tile).toEqual(tileBackup)
                    expect(input).toEqual([-0.65197, -0.85034, 2.10156])
                })
            })
            
            describe('over one edge', function(){
                beforeEach(function(){
                    input = [-2.27156, 0.58128, 1.89766]
                })                
                
                it('projects the point onto the edge', function(){
                    part.constrainToTile(tile, input, output = [])
                    expect(output.length).toEqual(3)
                    expect(output[0]).toBeCloseTo(-1.49466)
                    expect(output[1]).toBeCloseTo(0.0827)
                    expect(output[2]).toBeCloseTo(1.44706)
                })
                
                it('supports having the input as the output', function(){
                    part.constrainToTile(tile, input, input)
                    expect(input.length).toEqual(3)
                    expect(input[0]).toBeCloseTo(-1.49466)
                    expect(input[1]).toBeCloseTo(0.0827)
                    expect(input[2]).toBeCloseTo(1.44706)
                })                
                
                it('does not modify its inputs', function(){
                    part.constrainToTile(tile, input, [])
                    expect(tile).toEqual(tileBackup)
                    expect(input).toEqual([-2.27156, 0.58128, 1.89766])
                })            
            })
            
            describe('over two edges', function(){
                beforeEach(function(){
                    input = [-2.92829, 0.06657, 1.33491]
                })                    
                
                it('projects the point onto the border between the edges', function(){
                    part.constrainToTile(tile, input, output = [])
                    expect(output.length).toEqual(3)
                    expect(output[0]).toBeCloseTo(-1.76879)
                    expect(output[1]).toBeCloseTo(-0.13216)
                    expect(output[2]).toBeCloseTo(1.21215)
                })
                
                it('supports having the input as the output', function(){
                    part.constrainToTile(tile, input, input)
                    expect(input.length).toEqual(3)
                    expect(input[0]).toBeCloseTo(-1.76879)
                    expect(input[1]).toBeCloseTo(-0.13216)
                    expect(input[2]).toBeCloseTo(1.21215)
                })                
                
                it('does not modify its inputs', function(){
                    part.constrainToTile(tile, input, [])
                    expect(tile).toEqual(tileBackup)
                    expect(input).toEqual([-2.92829, 0.06657, 1.33491])
                })            
            })        
        })
        
        describe('below the surface', function(){
            describe('not over an edge', function(){
                beforeEach(function(){
                    input = [-0.52784, -0.21329, 1.207]
                })
                
                it('outputs the same point', function(){
                    part.constrainToTile(tile, input, output = [])
                    expect(output.length).toEqual(3)
                    expect(output[0]).toBeCloseTo(-0.74178)
                    expect(output[1]).toBeCloseTo(-0.39597)
                    expect(output[2]).toBeCloseTo(1.62375)
                })
                
                it('supports having the input as the output', function(){
                    part.constrainToTile(tile, input, input)
                    expect(input.length).toEqual(3)
                    expect(input[0]).toBeCloseTo(-0.74178)
                    expect(input[1]).toBeCloseTo(-0.39597)
                    expect(input[2]).toBeCloseTo(1.62375)
                })
                
                it('does not modify its inputs', function(){
                    part.constrainToTile(tile, input, [])
                    expect(tile).toEqual(tileBackup)
                    expect(input).toEqual([-0.52784, -0.21329, 1.207])
                })
            })
            
            describe('over one edge', function(){
                beforeEach(function(){
                    input = [-1.89376, 0.90387, 1.16174]
                })                
                
                it('projects the point onto the edge', function(){
                    part.constrainToTile(tile, input, output = [])
                    expect(output.length).toEqual(3)
                    expect(output[0]).toBeCloseTo(-1.49466)
                    expect(output[1]).toBeCloseTo(0.0827)
                    expect(output[2]).toBeCloseTo(1.44706)
                })
                
                it('supports having the input as the output', function(){
                    part.constrainToTile(tile, input, input)
                    expect(input.length).toEqual(3)
                    expect(input[0]).toBeCloseTo(-1.49466)
                    expect(input[1]).toBeCloseTo(0.0827)
                    expect(input[2]).toBeCloseTo(1.44706)
                })                
                
                it('does not modify its inputs', function(){
                    part.constrainToTile(tile, input, [])
                    expect(tile).toEqual(tileBackup)
                    expect(input).toEqual([-1.89376, 0.90387, 1.16174])
                })            
            })
            
            describe('over two edges', function(){
                beforeEach(function(){
                    input = [-2.66203, 0.30174, 0.5034]
                })                    
                
                it('projects the point onto the border between the edges', function(){
                    part.constrainToTile(tile, input, output = [])
                    expect(output.length).toEqual(3)
                    expect(output[0]).toBeCloseTo(-1.76879)
                    expect(output[1]).toBeCloseTo(-0.13216)
                    expect(output[2]).toBeCloseTo(1.21215)
                })
                
                it('supports having the input as the output', function(){
                    part.constrainToTile(tile, input, input)
                    expect(input.length).toEqual(3)
                    expect(input[0]).toBeCloseTo(-1.76879)
                    expect(input[1]).toBeCloseTo(-0.13216)
                    expect(input[2]).toBeCloseTo(1.21215)
                })                
                
                it('does not modify its inputs', function(){
                    part.constrainToTile(tile, input, [])
                    expect(tile).toEqual(tileBackup)
                    expect(input).toEqual([-2.66203, 0.30174, 0.5034])
                })            
            })        
        })      
    })   
    
    describe('tileDistance', function(){
        var geometry = {
            vertices: [
                [-1.76879, -0.13216, 1.21215],
                [-0.67803, 0.72273, 2.14684],
                [0.18378, -0.75249, 1.94261],
                [-0.81961, -1.70416, 1.01036]
            ],
            faces: [
                [0, 1, 2, 3]
            ]
        }
        var tile, tileBackup, input, result
        beforeEach(function(){
            tile = part.create(geometry)[0]
            tileBackup = part.create(geometry)[0]
        })
        
        describe('above the surface', function(){
            describe('not over an edge', function(){
                beforeEach(function(){
                    result = part.tileDistance(tile, input = [-0.65197, -0.85034, 2.10156])
                })
                
                it('projects the point onto the surface', function(){
                    expect(result).toBeCloseTo(0.522892)
                })
                
                it('does not modify its inputs', function(){
                    expect(input).toEqual([-0.65197, -0.85034, 2.10156])
                })
            })
            
            describe('over one edge', function(){
                beforeEach(function(){
                    result = part.tileDistance(tile, input = [-2.27156, 0.58128, 1.89766])
                })                
                
                it('projects the point onto the surface on the edge', function(){
                    expect(result).toBeCloseTo(1.027229)
                })
                
                it('does not modify its inputs', function(){
                    expect(input).toEqual([-2.27156, 0.58128, 1.89766])
                })              
            })
            
            describe('over two edges', function(){
                beforeEach(function(){
                    result = part.tileDistance(tile, input = [-2.92829, 0.06657, 1.33491])
                })                
                
                it('projects the point onto the surface on the edge', function(){
                    expect(result).toBeCloseTo(1.182790)
                })              
                
                it('does not modify its inputs', function(){
                    expect(input).toEqual([-2.92829, 0.06657, 1.33491])
                })            
            })        
        })
        
        describe('below the surface', function(){
            describe('not over an edge', function(){
                beforeEach(function(){
                    result = part.tileDistance(tile, input = [-0.52784, -0.21329, 1.207])
                })
                
                it('projects the point onto the surface', function(){
                    expect(result).toBeCloseTo(0.502811)
                })
                
                it('does not modify its inputs', function(){
                    expect(input).toEqual([-0.52784, -0.21329, 1.207])
                })
            })
            
            describe('over one edge', function(){
                beforeEach(function(){
                    result = part.tileDistance(tile, input = [-1.89376, 0.90387, 1.16174])
                })
                
                it('projects the point onto the surface', function(){
                    expect(result).toBeCloseTo(0.956560)
                })
                
                it('does not modify its inputs', function(){
                    expect(input).toEqual([-1.89376, 0.90387, 1.16174])
                })        
            })
            
            describe('over two edges', function(){
                beforeEach(function(){
                    result = part.tileDistance(tile, input = [-2.66203, 0.30174, 0.5034])
                })
                
                it('projects the point onto the surface', function(){
                    expect(result).toBeCloseTo(1.220030)
                })
                
                it('does not modify its inputs', function(){
                    expect(input).toEqual([-2.66203, 0.30174, 0.5034])
                })            
            })        
        })      
    })   
    
    describe('closestTile', function(){
        var navmesh, navmeshBackup, input, result
        describe('when comparing horizontally', function(){
            beforeEach(function(){
                var geometry = {
                    vertices: [
                        [1, 1, 0],
                        [1, -1, 0],
                        [-1, 1, 0],
                        [-1, -1, 0],
                        [-1, -3, 0],
                        [-3, 2, -1],
                        [-3, -1, -1],
                        [-3, -3, -1],
                        [-5, -3, -1],
                        [-7, 2, -1],
                        [-7, -1, -1]
                    ],
                    faces: [
                        [2, 0, 1, 3],
                        [1, 4, 3],
                        [3, 4, 7, 6],
                        [6, 7, 8, 10],
                        [5, 6, 10, 9]
                    ]
                }
                navmesh = part.create(geometry)
                navmeshBackup = part.create(geometry)
            })
            
            describe('when inside the tile\'s edges', function(){
                beforeEach(function(){
                    result = part.closestTile(navmesh, input = [-3.95825, -1.50799, -0.19205])
                })
                
                it('returns the closest tile', function(){
                    expect(result).toBe(navmesh[3])
                })
                
                it('does not modify its inputs', function(){
                    expect(navmesh).toEqual(navmeshBackup)
                    expect(input).toEqual([-3.95825, -1.50799, -0.19205])
                })
            })
            
            describe('when outside of its edges', function(){
                beforeEach(function(){
                    result = part.closestTile(navmesh, input = [-1.8529, -0.70146, -0.19205])                    
                })
                
                it('returns the closest tile', function(){
                    expect(result).toBe(navmesh[2])
                })
                
                it('does not modify its inputs', function(){
                    expect(navmesh).toEqual(navmeshBackup)
                    expect(input).toEqual([-1.8529, -0.70146, -0.19205])
                })
            })   
        })
        
        describe('when multiple tiles are equally eligible horizontally', function(){
            beforeEach(function(){
                var geometry = {
                    vertices: [
                        [-5, -5, 4],
                        [-1, -5, 4],
                        [-1, -8, 4],
                        [-5, -8, 4],
                        [-5, -5, 3],
                        [-1, -5, 3],
                        [-1, -8, 3],
                        [-5, -8, 3],
                        [-5, -5, 0],
                        [-1, -5, 0],
                        [-1, -8, 0],
                        [-5, -8, 0],
                        [-5, -5, -1],
                        [-1, -5, -1],
                        [-1, -8, -1],
                        [-5, -8, -1]                        
                    ],
                    faces: [
                        [0, 1, 2, 3],
                        [4, 5, 6, 7],
                        [8, 9, 10, 11],
                        [12, 13, 14, 15]
                    ]
                }
                navmesh = part.create(geometry)
                navmeshBackup = part.create(geometry)
            })
            
            describe('when inside their edges', function(){
                beforeEach(function(){
                    result = part.closestTile(navmesh, input = [-2.52385, -7.46171, 0.47187])  
                })
                
                it('returns the closest tile vertically', function(){
                    expect(result).toBe(navmesh[2])
                })
                
                it('does not modify its inputs', function(){
                    expect(navmesh).toEqual(navmeshBackup)
                    expect(input).toEqual([-2.52385, -7.46171, 0.47187])                    
                })
            })   
            
            describe('when outside their edges', function(){
                beforeEach(function(){
                    result = part.closestTile(navmesh, input = [-2.52385, -8.46171, 3.26511])  
                })
                
                it('returns the closest tile vertically', function(){
                    expect(result).toBe(navmesh[1])
                })
                
                it('does not modify its inputs', function(){
                    expect(navmesh).toEqual(navmeshBackup)
                    expect(input).toEqual([-2.52385, -8.46171, 3.26511])                    
                })
            })            
        })
    })
})