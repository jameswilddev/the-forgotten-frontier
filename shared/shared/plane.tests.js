describe('plane', function(){
    var plane
    beforeEach(function(){
        plane = parts.plane({
            vector: parts.vector()
        })
    })
    
    describe('create', function(){
        var result
        beforeEach(function(){
            plane.create([
                [-1.12274, 2.19943, 11.1308],
                [-0.95796, 3.46643, 9.57391],
                [0.62898, 2.48774, 9.80567],
                [-0.03448, 1.15932, 11.71593]
            ], result = {})
        })
        it('finds the normal of the input', function(){
            expect(result.normal.length).toEqual(3)
            expect(result.normal[0]).toBeCloseTo(0.34757)
            expect(result.normal[1]).toBeCloseTo(0.70891)
            expect(result.normal[2]).toBeCloseTo(0.6137)
        })
        it('finds the distance from zero', function(){
            expect(result.distance).toBeCloseTo(8)
        })
    })
    
    describe('distance', function(){
        it('returns the positive distance when in front of the plane', function(){
            expect(plane.distance({
                normal: [0.34757, 0.70891, 0.6137],
                distance: 8
            }, [0.06192, 4.60748, 12.56657])).toBeCloseTo(3)
        })
        it('returns the negative distance when behind the plane', function(){
            expect(plane.distance({
                normal: [0.34757, 0.70891, 0.6137],
                distance: 15
            }, [0.06192, 4.60748, 12.56657])).toBeCloseTo(-4)
        })        
        it('does not modify the inputs', function(){
            var inputA = {
                normal: [0.34757, 0.70891, 0.6137],
                distance: 8
            }
            var inputB = [0.06192, 4.60748, 12.56657]
            
            plane.distance(inputA, inputB)
            
            expect(inputA).toEqual({
                normal: [0.34757, 0.70891, 0.6137],
                distance: 8
            })
            expect(inputB).toEqual([0.06192, 4.60748, 12.56657])
        })        
    })
    
    describe('before', function(){
        it('returns true when the point is in front of the plane', function(){
            expect(plane.before({
                normal: [0.34757, 0.70891, 0.6137],
                distance: 8
            }, [0.06192, 4.60748, 12.56657])).toBeTruthy()
        })
        it('returns false when the point is behind the plane', function(){
            expect(plane.before({
                normal: [0.34757, 0.70891, 0.6137],
                distance: 14
            }, [0.06192, 4.60748, 12.56657])).toBeFalsy()
        })        
        it('does not modify the inputs', function(){
            var inputA = {
                normal: [0.34757, 0.70891, 0.6137],
                distance: 8
            }
            var inputB = [0.06192, 4.60748, 12.56657]
            
            plane.before(inputA, inputB)
            
            expect(inputA).toEqual({
                normal: [0.34757, 0.70891, 0.6137],
                distance: 8
            })
            expect(inputB).toEqual([0.06192, 4.60748, 12.56657])
        })        
    })    
    
    describe('behind', function(){
        it('returns false when the point is in front of the plane', function(){
            expect(plane.behind({
                normal: [0.34757, 0.70891, 0.6137],
                distance: 8
            }, [0.06192, 4.60748, 12.56657])).toBeFalsy()
        })
        it('returns true when the point is behind the plane', function(){
            expect(plane.behind({
                normal: [0.34757, 0.70891, 0.6137],
                distance: 14
            }, [0.06192, 4.60748, 12.56657])).toBeTruthy()
        })        
        it('does not modify the inputs', function(){
            var inputA = {
                normal: [0.34757, 0.70891, 0.6137],
                distance: 8
            }
            var inputB = [0.06192, 4.60748, 12.56657]
            
            plane.behind(inputA, inputB)
            
            expect(inputA).toEqual({
                normal: [0.34757, 0.70891, 0.6137],
                distance: 8
            })
            expect(inputB).toEqual([0.06192, 4.60748, 12.56657])
        })        
    })       
    
    describe('project', function(){
        it('projects the given point onto the plane', function(){
            var result = []
            plane.project({
                normal: [0.34757, 0.70891, 0.6137],
                distance: 8
            }, [0.06192, 4.60748, 12.56657], result)
            
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(-0.9808)
            expect(result[1]).toBeCloseTo(2.48074)
            expect(result[2]).toBeCloseTo(10.72545)
        })
        it('does not modify the inputs', function(){
            var inputA = {
                normal: [0.34757, 0.70891, 0.6137],
                distance: 8
            }
            var inputB = [0.06192, 4.60748, 12.56657]
            
            plane.project(inputA, inputB, [])
            
            expect(inputA).toEqual({
                normal: [0.34757, 0.70891, 0.6137],
                distance: 8
            })
            expect(inputB).toEqual([0.06192, 4.60748, 12.56657])
        })
        it('supports the input and output being the same reference', function(){
            var result = [0.06192, 4.60748, 12.56657]
            plane.project({
                normal: [0.34757, 0.70891, 0.6137],
                distance: 8
            }, result, result)
            
            expect(result.length).toEqual(3)
            expect(result[0]).toBeCloseTo(-0.9808)
            expect(result[1]).toBeCloseTo(2.48074)
            expect(result[2]).toBeCloseTo(10.72545)            
        })
    })
    
    describe('intersect', function(){
        it('supports approaching the plane from the front', function(){
            expect(plane.intersect({
                normal: [0.26405, 0.96451],
                distance: 3
            }, [2.15273, 4.07624], [1.66129, 3.69238])).toBeCloseTo(3)
        })
        
        it('supports intersecting the plane from the front', function(){
            expect(plane.intersect({
                normal: [0.26405, 0.96451],
                distance: 3
            }, [1.66129, 3.69238], [-0.30438, 2.67532])).toBeCloseTo(0.666666)                                      
        })
        
        it('supports abandoning the plane from the front', function(){
            expect(plane.intersect({
                normal: [0.26405, 0.96451],
                distance: 3
            }, [1.66129, 3.69238], [2.15273, 4.07624])).toBeCloseTo(-2)            
        })
        
        it('supports approaching the plane from the back', function(){
            expect(plane.intersect({
                normal: [0.26405, 0.96451],
                distance: 3
            }, [1.1332, 1.76336], [-0.30438, 2.67532])).toBeCloseTo(2)                        
        })
        
        it('supports intersecting the plane from the back', function(){
            expect(plane.intersect({
                normal: [0.26405, 0.96451],
                distance: 3
            }, [-0.30438, 2.67532], [1.66129, 3.69238])).toBeCloseTo(0.333333)                          
        })
        
        it('supports abandoning the plane from the back', function(){
            expect(plane.intersect({
                normal: [0.26405, 0.96451],
                distance: 3
            }, [-0.30438, 2.67532], [1.1332, 1.76336])).toBeCloseTo(-1)              
        })
        
        it('does not modify its inputs', function(){
            var inputA = {
                normal: [0.26405, 0.96451],
                distance: 3
            }
            var inputB = [-0.30438, 2.67532]
            var inputC = [1.1332, 1.76336]
            
            plane.intersect(inputA, inputB, inputC)
            
            expect(inputA).toEqual({
                normal: [0.26405, 0.96451],
                distance: 3
            })
            expect(inputB).toEqual([-0.30438, 2.67532])
            expect(inputC).toEqual([1.1332, 1.76336])
        })
    })
})