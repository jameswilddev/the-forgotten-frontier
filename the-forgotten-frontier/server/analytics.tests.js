describe('analytics', function(){
    var rewire = require('rewire')
    var module, sqlite3, sqlite3instance, db, now, uuid, validator, configuration, app
    
    beforeEach(function(){
        module = rewire('./analytics.js')
        app = {
            post: jasmine.createSpy()
        }
        module.__set__('sqlite3', sqlite3 = {
            verbose: jasmine.createSpy()
        })
        
        sqlite3.verbose.and.returnValue(sqlite3instance = {
            Database: jasmine.createSpy()
        })
        sqlite3instance.Database.and.returnValue(db = {
            serialize: jasmine.createSpy()
        })
        module.__set__('queries', queries = {
            enableForeignKeys: 'Test Enable Foreign Keys',
            createAnalyticsKey: 'Test Create Analytics Key',
            createAnalyticsError: 'Test Create Analytics Error',
            createAnalyticsUpdate: 'Test Create Analytics Update'
        })
        
        module.__set__('uuid', uuid = {})
        module.__set__('validator', validator = {})
        module.__set__('configuration', configuration = {
            baseUrl: 'Test Base Url',
            db: 'Test Connection String'
        })
    })    
    
    it('imports date.now', function(){
        expect(module.__get__('now')).toBe(Date.now)
    })
    
    it('imports console.log', function(){
        expect(module.__get__('log')).toBe(console.log)
    })    
    
    describe('on running', function(){
        var log
        beforeEach(function(){
            module.__set__('log', log = jasmine.createSpy())
            module(app)            
        })
        
        it('starts sqlite3 in verbose mode', function(){
            expect(sqlite3.verbose).toHaveBeenCalled()
        })
        
        it('then creates a new database context using the connection string from the configuration', function(){
            expect(sqlite3instance.Database).toHaveBeenCalledWith('Test Connection String')
        })
        
        it('then serializes database setup', function(){
            expect(db.serialize).toHaveBeenCalled()
        })
        
        describe('database setup', function(){
            beforeEach(function(){
                db.run = jasmine.createSpy()
            })
            it('enables foreign keys', function(){
                db.serialize.calls.argsFor(0)[0]()
                expect(db.run).toHaveBeenCalledWith('Test Enable Foreign Keys')
            })        
            
            it('creates the analytics key table', function(){
                db.run.and.callFake(function(query){
                    if(query == 'Test Create Analytics Key')
                        expect(db.run).toHaveBeenCalledWith('Test Enable Foreign Keys')
                })
                db.serialize.calls.argsFor(0)[0]()
                expect(db.run).toHaveBeenCalledWith('Test Create Analytics Key')
            })
            
            it('then creates the analytics error table', function(){
                db.run.and.callFake(function(query){
                    if(query == 'Test Create Analytics Error')
                        expect(db.run).toHaveBeenCalledWith('Test Create Analytics Key')
                })
                db.serialize.calls.argsFor(0)[0]()
                expect(db.run).toHaveBeenCalledWith('Test Create Analytics Error')            
            })
            
            it('then creates then analytics update table', function(){
                db.run.and.callFake(function(query){
                    if(query == 'Test Create Analytics Update')
                        expect(db.run).toHaveBeenCalledWith('Test Create Analytics Key')
                })
                db.serialize.calls.argsFor(0)[0]()
                expect(db.run).toHaveBeenCalledWith('Test Create Analytics Update')            
            })
        })
        
        describe('routing', function(){
            it('creates a start route', function(){
                expect(app.post).toHaveBeenCalledWith('/Test Base Url/analytics/start', jasmine.any(Function))
            })
            
            describe('start', function(){
                var handler, req, res
                beforeEach(function(){
                    for(var index = 0; index < app.post.calls.count(); index++)
                        if(app.post.calls.argsFor(index)[0] == '/Test Base Url/analytics/start')
                            handler = app.post.calls.argsFor(index)[1]
                            
                    req = {
                        headers: {
                            'user-agent': 'Test User Agent'
                        }
                    }
                    
                    res = {
                    }
                    
                    db.run = jasmine.createSpy()
                            
                    uuid.v4 = jasmine.createSpy()
                    uuid.v4.and.returnValue('Test UUID')
                })
                
                it('generates a uuid', function(){
                    handler(req, res)
                    expect(uuid.v4).toHaveBeenCalled()
                })
                
                it('writes the uuid, time and user agent to the key table', function(){
                    module.__set__('now', now = jasmine.createSpy())
                    now.and.returnValue(1428706086609)
                    queries.insertAnalyticsKey = 'Test Insert Analytics Key'
                    
                    handler(req, res)
                    expect(db.run).toHaveBeenCalledWith('Test Insert Analytics Key', 'Test UUID', 1428706086609, 'Test User Agent', jasmine.any(Function))
                })
                
                describe('on completing the write successfully', function(){
                    beforeEach(function(){
                        handler(req, res)
                        res.json = jasmine.createSpy()
                        db.run.calls.argsFor(0)[4]()
                    })
                    
                    it('sends the uuid back to the client', function(){
                        expect(res.json).toHaveBeenCalledWith('Test UUID')
                    })
                })
                
                describe('on encountering an error completing the write', function(){
                    beforeEach(function(){
                        handler(req, res)
                        res.status = jasmine.createSpy()
                        res.end = jasmine.createSpy()
                    })          
                    
                    it('logs the error', function(){
                        db.run.calls.argsFor(0)[4]('Test Error')
                        expect(log).toHaveBeenCalledWith('Test Error')
                    })
                    
                    it('sets the status code to inform the client of an internal error', function(){
                        db.run.calls.argsFor(0)[4]('Test Error')
                        expect(res.status).toHaveBeenCalledWith(500)
                    })
                    
                    it('then completes the request', function(){
                        res.end.and.callFake(function(){
                            expect(res.status).toHaveBeenCalled()
                        })
                        db.run.calls.argsFor(0)[4]('Test Error')
                        expect(res.end).toHaveBeenCalled()
                    })
                })
            })
            
            it('creates an error route', function(){
                expect(app.post).toHaveBeenCalledWith('/Test Base Url/analytics/error', jasmine.any(Function))
            })        
            
            describe('error', function(){
                var handler, req, res
                beforeEach(function(){
                    for(var index = 0; index < app.post.calls.count(); index++)
                        if(app.post.calls.argsFor(index)[0] == '/Test Base Url/analytics/error')
                            handler = app.post.calls.argsFor(index)[1]
                            
                    req = {
                        body: {
                            key: 'Test Key',
                            details: 'Test Details'
                        }
                    }
                    
                    res = {
                    }
                    
                    db.run = jasmine.createSpy()
                    
                    validator.isUUID = jasmine.createSpy()
                    validator.isUUID.and.returnValue(true)
                })            
                
                it('validates the uuid', function(){
                    handler(req, res)
                    expect(validator.isUUID).toHaveBeenCalledWith('Test Key')
                })
                
                describe('when the uuid is invalid', function(){
                    beforeEach(function(){
                        validator.isUUID.and.callFake(function(uuid){
                            return uuid != 'Test Key'
                        })
                        res.status = jasmine.createSpy()
                        res.end = jasmine.createSpy()
                    })                
                    
                    it('sets the status code to inform the client their request was invalid', function(){
                        handler(req, res)
                        expect(res.status).toHaveBeenCalledWith(400)
                    })
                    
                    it('then completes the request', function(){
                        res.end.and.callFake(function(){
                            expect(res.status).toHaveBeenCalled()
                        })
                        handler(req, res)
                        expect(res.end).toHaveBeenCalled()
                    })
                })
                
                describe('when the uuid is valid', function(){
                    beforeEach(function(){
                        queries.insertAnalyticsError = 'Test Insert Analytics Error'
                        db.run = jasmine.createSpy()
                        module.__set__('now', now = jasmine.createSpy())
                        now.and.returnValue(1428706086609)                    
                        handler(req, res)
                    })
                    
                    it('attempts to record the error', function(){
                        expect(db.run).toHaveBeenCalledWith('Test Insert Analytics Error', 'Test Key', 1428706086609, 'Test Details', jasmine.any(Function))
                    })
                    
                    describe('on completing the write successfully', function(){
                        beforeEach(function(){
                            res.end = jasmine.createSpy()
                        })                       
        
                        it('completes the request', function(){
                            db.run.calls.argsFor(0)[4]()
                            expect(res.end).toHaveBeenCalled()
                        })
                    })
        
                    describe('when the client\'s key is not recognized', function(){
                        beforeEach(function(){
                            res.status = jasmine.createSpy()
                            res.end = jasmine.createSpy()
                        })                
                        
                        it('sets the status code to inform the client that their request was invalid', function(){
                            db.run.calls.argsFor(0)[4]({code: 'SQLITE_CONSTRAINT'})
                            expect(res.status).toHaveBeenCalledWith(400)
                        })
                        
                        it('then completes the request', function(){
                            res.end.and.callFake(function(){
                                expect(res.status).toHaveBeenCalled()
                            })
                            db.run.calls.argsFor(0)[4]({code: 'SQLITE_CONSTRAINT'})
                            expect(res.end).toHaveBeenCalled()
                        })
                    })
                    
                    describe('on encountering an unexpected error completing the write', function(){
                        beforeEach(function(){
                            res.status = jasmine.createSpy()
                            res.end = jasmine.createSpy()
                        })    
                        
                        it('logs the error', function(){
                            db.run.calls.argsFor(0)[4]('Test Error')
                            expect(log).toHaveBeenCalledWith('Test Error')
                        })                        
                        
                        it('sets the status code to inform the client of an internal error', function(){
                            db.run.calls.argsFor(0)[4]({code: 'Test Error'})
                            expect(res.status).toHaveBeenCalledWith(500)
                        })
                        
                        it('then completes the request', function(){
                            res.end.and.callFake(function(){
                                expect(res.status).toHaveBeenCalled()
                            })
                            db.run.calls.argsFor(0)[4]({code: 'Test Error'})
                            expect(res.end).toHaveBeenCalled()
                        })
                    })
                })
            })
            
            it('creates an update route', function(){
                expect(app.post).toHaveBeenCalledWith('/Test Base Url/analytics/update', jasmine.any(Function))
            })              
            
            describe('update', function(){
                var handler, req, res
                beforeEach(function(){
                    for(var index = 0; index < app.post.calls.count(); index++)
                        if(app.post.calls.argsFor(index)[0] == '/Test Base Url/analytics/update')
                            handler = app.post.calls.argsFor(index)[1]
                            
                    req = {
                        body: {
                            key: 'Test Key',
                            x: 'Test X',
                            y: 'Test Y',
                            z: 'Test Z',
                            pitch: 'Test Pitch',
                            yaw: 'Test Yaw',
                            framerate: 'Test Framerate'
                        }
                    }
                    
                    res = {
                    }
                    
                    db.run = jasmine.createSpy()
                    
                    validator.isUUID = jasmine.createSpy()
                    validator.isUUID.and.returnValue(true)
                    
                    validator.toFloat = jasmine.createSpy()
                    validator.toFloat.and.callFake(function(input){
                        switch(input) {
                            case 'Test X': return 2.64
                            case 'Test Y': return 5.4
                            case 'Test Z': return 3.8
                            case 'Test Pitch': return 0.8
                            case 'Test Yaw': return 225.3
                        }
                        return NaN
                    })                    
                    
                    validator.toInt = jasmine.createSpy()
                    validator.toInt.and.callFake(function(input){
                        switch(input) {
                            case 'Test Framerate': return 44
                        }
                        return NaN
                    })                    
                })             
                
                it('validates the uuid', function(){
                    handler(req, res)
                    expect(validator.isUUID).toHaveBeenCalledWith('Test Key')
                })
                
                describe('when the uuid is invalid', function(){
                    beforeEach(function(){
                        validator.isUUID.and.callFake(function(uuid){
                            return uuid != 'Test Key'
                        })
                        res.status = jasmine.createSpy()
                        res.end = jasmine.createSpy()
                    })                
                    
                    it('sets the status code to inform the client their request was invalid', function(){
                        handler(req, res)
                        expect(res.status).toHaveBeenCalledWith(400)
                    })
                    
                    it('then completes the request', function(){
                        res.end.and.callFake(function(){
                            expect(res.status).toHaveBeenCalled()
                        })
                        handler(req, res)
                        expect(res.end).toHaveBeenCalled()
                    })
                })
                
                it('validates the x location', function(){
                    handler(req, res)
                    expect(validator.toFloat).toHaveBeenCalledWith('Test X')                
                })
                
                describe('when the x location is invalid', function(){
                    beforeEach(function(){
                        validator.toFloat.and.callFake(function(uuid){
                            if(uuid != 'Test X') return 3.1
                            return NaN
                        })
                        res.status = jasmine.createSpy()
                        res.end = jasmine.createSpy()
                    })                
                    
                    it('sets the status code to inform the client their request was invalid', function(){
                        handler(req, res)
                        expect(res.status).toHaveBeenCalledWith(400)
                    })
                    
                    it('then completes the request', function(){
                        res.end.and.callFake(function(){
                            expect(res.status).toHaveBeenCalled()
                        })
                        handler(req, res)
                        expect(res.end).toHaveBeenCalled()
                    })
                })            
                
                it('validates the y location', function(){
                    handler(req, res)
                    expect(validator.toFloat).toHaveBeenCalledWith('Test Y')                                
                })
                
                describe('when the y location is invalid', function(){
                    beforeEach(function(){
                        validator.toFloat.and.callFake(function(uuid){
                            if(uuid != 'Test Y') return 3.1
                            return NaN
                        })
                        res.status = jasmine.createSpy()
                        res.end = jasmine.createSpy()
                    })                
                    
                    it('sets the status code to inform the client their request was invalid', function(){
                        handler(req, res)
                        expect(res.status).toHaveBeenCalledWith(400)
                    })
                    
                    it('then completes the request', function(){
                        res.end.and.callFake(function(){
                            expect(res.status).toHaveBeenCalled()
                        })
                        handler(req, res)
                        expect(res.end).toHaveBeenCalled()
                    })
                })     
                
                it('validates the z location', function(){
                    handler(req, res)
                    expect(validator.toFloat).toHaveBeenCalledWith('Test Z')                                
                })
                
                describe('when the z location is invalid', function(){
                    beforeEach(function(){
                        validator.toFloat.and.callFake(function(uuid){
                            if(uuid != 'Test Z') return 3.1
                            return NaN
                        })
                        res.status = jasmine.createSpy()
                        res.end = jasmine.createSpy()
                    })                
                    
                    it('sets the status code to inform the client their request was invalid', function(){
                        handler(req, res)
                        expect(res.status).toHaveBeenCalledWith(400)
                    })
                    
                    it('then completes the request', function(){
                        res.end.and.callFake(function(){
                            expect(res.status).toHaveBeenCalled()
                        })
                        handler(req, res)
                        expect(res.end).toHaveBeenCalled()
                    })
                })            
                
                it('validates the pitch', function(){
                    handler(req, res)
                    expect(validator.toFloat).toHaveBeenCalledWith('Test Pitch')                                
                })
                
                describe('when the pitch is invalid', function(){
                    beforeEach(function(){
                        validator.toFloat.and.callFake(function(uuid){
                            if(uuid != 'Test Pitch') return 3.1
                            return NaN
                        })
                        res.status = jasmine.createSpy()
                        res.end = jasmine.createSpy()
                    })                
                    
                    it('sets the status code to inform the client their request was invalid', function(){
                        handler(req, res)
                        expect(res.status).toHaveBeenCalledWith(400)
                    })
                    
                    it('then completes the request', function(){
                        res.end.and.callFake(function(){
                            expect(res.status).toHaveBeenCalled()
                        })
                        handler(req, res)
                        expect(res.end).toHaveBeenCalled()
                    })
                })           
                
                it('validates the yaw', function(){
                    handler(req, res)
                    expect(validator.toFloat).toHaveBeenCalledWith('Test Yaw')                                
                })
                
                describe('when the yaw is invalid', function(){
                    beforeEach(function(){
                        validator.toFloat.and.callFake(function(uuid){
                            if(uuid != 'Test Yaw') return 3.1
                            return NaN
                        })
                        res.status = jasmine.createSpy()
                        res.end = jasmine.createSpy()
                    })                
                    
                    it('sets the status code to inform the client their request was invalid', function(){
                        handler(req, res)
                        expect(res.status).toHaveBeenCalledWith(400)
                    })
                    
                    it('then completes the request', function(){
                        res.end.and.callFake(function(){
                            expect(res.status).toHaveBeenCalled()
                        })
                        handler(req, res)
                        expect(res.end).toHaveBeenCalled()
                    })
                }) 
                
                it('validates the framerate', function(){
                    handler(req, res)
                    expect(validator.toInt).toHaveBeenCalledWith('Test Framerate')                                
                })
                
                describe('when the framerate is invalid', function(){
                    beforeEach(function(){
                        validator.toInt.and.callFake(function(uuid){
                            if(uuid != 'Test Framerate') return 3.1
                            return NaN
                        })
                        res.status = jasmine.createSpy()
                        res.end = jasmine.createSpy()
                    })                
                    
                    it('sets the status code to inform the client their request was invalid', function(){
                        handler(req, res)
                        expect(res.status).toHaveBeenCalledWith(400)
                    })
                    
                    it('then completes the request', function(){
                        res.end.and.callFake(function(){
                            expect(res.status).toHaveBeenCalled()
                        })
                        handler(req, res)
                        expect(res.end).toHaveBeenCalled()
                    })
                })                
                
                describe('when the uuid and values are valid', function(){
                    beforeEach(function(){
                        queries.insertAnalyticsUpdate = 'Test Insert Analytics Update'
                        module.__set__('now', now = jasmine.createSpy())
                        now.and.returnValue(1428706086609)     
                        handler(req, res)                    
                    })
                    
                    it('attempts to record the update', function(){
                        expect(db.run).toHaveBeenCalledWith(
                            'Test Insert Analytics Update', 
                            'Test Key', 
                            1428706086609, 
                            2.64,
                            5.4,
                            3.8, 
                            0.8,
                            225.3, 
                            44, 
                            jasmine.any(Function)
                        )
                    })
                    
                    describe('on completing the write successfully', function(){
                        beforeEach(function(){
                            res.end = jasmine.createSpy()
                        })                       
        
                        it('completes the request', function(){
                            db.run.calls.argsFor(0)[9]()
                            expect(res.end).toHaveBeenCalled()
                        })
                    })
        
                    describe('when the client\'s key is not recognized', function(){
                        beforeEach(function(){
                            res.status = jasmine.createSpy()
                            res.end = jasmine.createSpy()
                        })                
                        
                        it('sets the status code to inform the client that their request was invalid', function(){
                            db.run.calls.argsFor(0)[9]({code: 'SQLITE_CONSTRAINT'})
                            expect(res.status).toHaveBeenCalledWith(400)
                        })
                        
                        it('then completes the request', function(){
                            res.end.and.callFake(function(){
                                expect(res.status).toHaveBeenCalled()
                            })
                            db.run.calls.argsFor(0)[9]({code: 'SQLITE_CONSTRAINT'})
                            expect(res.end).toHaveBeenCalled()
                        })
                    })
                    
                    describe('on encountering an unexpected error completing the write', function(){
                        beforeEach(function(){
                            res.status = jasmine.createSpy()
                            res.end = jasmine.createSpy()
                        })                

                        it('logs the error', function(){
                            db.run.calls.argsFor(0)[9]('Test Error')
                            expect(log).toHaveBeenCalledWith('Test Error')
                        })
                        
                        it('sets the status code to inform the client of an internal error', function(){
                            db.run.calls.argsFor(0)[9]({code: 'Test Error'})
                            expect(res.status).toHaveBeenCalledWith(500)
                        })
                        
                        it('then completes the request', function(){
                            res.end.and.callFake(function(){
                                expect(res.status).toHaveBeenCalled()
                            })
                            db.run.calls.argsFor(0)[9]({code: 'Test Error'})
                            expect(res.end).toHaveBeenCalled()
                        })
                    })
                })            
            })
        })
    })
})