var configuration = require('./configuration')
var path = require('path')
var sqlite3 = require('sqlite3')
var uuid = require('uuid')
var validator = require('validator')
var queries = require('./queries')
var now = Date.now
var log = console.log

// At present this service is hosted by a root NodeJS app which also serves 
// www.sunruse.co.uk's static content.
// Call with the express object to set up the database and routing.
module.exports = function(app){
  var instance = sqlite3.verbose()
  var db = new instance.Database(configuration.db)
  db.serialize(function(){
    db.run(queries.enableForeignKeys)
    db.run(queries.createAnalyticsKey)
    db.run(queries.createAnalyticsError)
    db.run(queries.createAnalyticsUpdate)    
  })
  
  // /{baseUrl}/analytics/error post with:
  // -key: the analytics key returned by /{baseUrl}/analytics/start.
  // -details: a string describing the error.
  // to record that an error has occurred.
  app.post('/' + path.join(configuration.baseUrl, 'analytics', 'error'), function (req, res) {
    if(!validator.isUUID(req.body.key)){
      res.status(400)
      res.end()
      return
    }
    db.run(queries.insertAnalyticsError, req.body.key, now(), req.body.details, function(err) {
      if(err) {
        log(err)
        res.status(err.code == 'SQLITE_CONSTRAINT' ? 400 : 500)
        res.end()
      } else {
        res.end()
      }
    })
  })
  
  // /{baseUrl}/analytics/update post with:
  // -key: the analytics key returned by /{baseUrl}/analytics/start.
  // -x: the location of the player on the x axis
  // -y: the location of the player on the y axis
  // -z: the location of the player on the z axis
  // -pitch: the view pitch (-1 -> 0 -> 1 from looking down to at the horizon to looking up)
  // -yaw: the view yaw, in radians, where 0.0 is along Y+
  // -framerate: the framerate, as an integer
  // to record an update of the player's state.
  app.post('/' + path.join(configuration.baseUrl, 'analytics', 'update'), function (req, res) {
    if(!validator.isUUID(req.body.key) 
    || isNaN(validator.toFloat(req.body.x)) 
    || isNaN(validator.toFloat(req.body.y)) 
    || isNaN(validator.toFloat(req.body.z))
    || isNaN(validator.toFloat(req.body.pitch))
    || isNaN(validator.toFloat(req.body.yaw))
    || isNaN(validator.toInt(req.body.framerate))){
      res.status(400)
      res.end()
      return
    }    
    db.run(queries.insertAnalyticsUpdate, req.body.key, now(), validator.toFloat(req.body.x), validator.toFloat(req.body.y), validator.toFloat(req.body.z), validator.toFloat(req.body.pitch), validator.toFloat(req.body.yaw), validator.toInt(req.body.framerate), function(err) {
      if(err) {
        log(err)
        res.status(err.code == 'SQLITE_CONSTRAINT' ? 400 : 500)
        res.end()
      } else {
        res.end()
      }
    })
  })  
  
  // /{baseUrl}/analytics/start post to get a new analytics key.
  app.post('/' + path.join(configuration.baseUrl, 'analytics', 'start'), function (req, res) {
    var id = uuid.v4()
    db.run(queries.insertAnalyticsKey, id, now(), req.headers['user-agent'], function(err) {
      if(err) {
        log(err)
        res.status(500)
        res.end()
      } else {
        res.json(id)
      }
    })
  })  
}


