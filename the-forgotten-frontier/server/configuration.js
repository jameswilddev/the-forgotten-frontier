var path = require('path')
exports.baseUrl = path.join('games', 'the-forgotten-frontier')
exports.db = path.join('games', 'the-forgotten-frontier', 'db')