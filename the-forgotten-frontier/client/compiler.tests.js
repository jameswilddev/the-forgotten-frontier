describe('compiler', function(){
    var instances
    beforeEach(function(){
        instances = {
            request: jasmine.createSpy(),
            configuration: {
                glslLibraries: ['Test Library A', 'Test Library B', null, 'Test Library C']
            }
        }
    })
    
    it('requests the first file', function(){
        parts.compiler(instances)('Test Scene')
        expect(instances.request).toHaveBeenCalledWith('glsl/Test Library A.glsl', 'GET', jasmine.any(Function))
    })
    
    describe('on completing the first file', function(){
        var callbackA, completion
        beforeEach(function(){
            instances.request.and.callFake(function(url, method, callback){
                if(url != 'glsl/Test Library A.glsl') return
                if(method != 'GET') return
                callbackA = callback
            })
            parts.compiler(instances)('Test Scene', completion = jasmine.createSpy())
        })
        
        it('downloads the second file', function(){
            instances.request.calls.reset()
            callbackA()
            expect(instances.request).toHaveBeenCalledWith('glsl/Test Library B.glsl', 'GET', jasmine.any(Function))
        })
        
        it('does not fire the completion callback', function(){
            expect(completion).not.toHaveBeenCalled()                
        })        
        
        describe('on completing the second file', function(){
            var callbackB
            beforeEach(function(){
                instances.request.and.callFake(function(url, method, callback){
                    if(url != 'glsl/Test Library B.glsl') return
                    if(method != 'GET') return
                    callbackB = callback
                })
                callbackA('Test Content A')
            })
            
            it('downloads the third file', function(){
                instances.request.calls.reset()
                callbackB()
                expect(instances.request).toHaveBeenCalledWith('glsl/Test Library C.glsl', 'GET', jasmine.any(Function))
            })
            
            it('does not fire the completion callback', function(){
                expect(completion).not.toHaveBeenCalled()                
            })
            
            describe('on completing the second file', function(){
                var callbackC
                beforeEach(function(){
                    instances.request.and.callFake(function(url, method, callback){
                        if(url != 'glsl/Test Library C.glsl') return
                        if(method != 'GET') return
                        callbackC = callback
                    })
                    callbackB('Test Content B')
                })
                
                it('calls the completion callback with the concatenated script', function(){
                    instances.request.calls.reset()
                    callbackC('Test Content C')
                    expect(completion.calls.count()).toEqual(1)
                    expect(completion).toHaveBeenCalledWith('Test Content A\nTest Content B\nTest Scene\nTest Content C')
                })
            })            
        })
    })
})