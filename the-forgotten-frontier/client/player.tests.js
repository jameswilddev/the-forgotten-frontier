describe('player', function(){
    var part, instances
    beforeEach(function(){
        part = parts.player(instances = {})
    })
    
    describe('create', function(){
        var result, returnedActor
        
        beforeEach(function(){
            instances.actor = {
                create: jasmine.createSpy()
            }
            
            instances.actor.create.and.returnValue(returnedActor = {})
            
            result = part.create('Test Navmesh', {
                location: 'Test Location',
                pitch: 'Test Pitch',
                yaw: 'Test Yaw'
            })
        })
        
        it('creates a new actor', function(){
            expect(instances.actor.create.calls.count()).toEqual(1)
            expect(instances.actor.create).toHaveBeenCalledWith('Test Navmesh', 'Test Location')
        })
        
        it('returns the new actor', function(){
            expect(result).toBe(returnedActor)
        })
        
        it('sets the pitch and yaw on the new actor', function(){
            expect(result).toEqual({
                yaw: 'Test Yaw',
                previousYaw: 'Test Yaw',
                pitch: 'Test Pitch',
                previousPitch: 'Test Pitch'
            })
        })
    })
    
    describe('tick', function(){
        var player, originalLocation
        beforeEach(function(){
            instances.vector = parts.vector(instances)
            
            instances.configuration = {
                player: {
                    pitchUpper: 0.45,
                    pitchLower: -0.36,
                    walkSpeed: 18.5,
                    strafeSpeed: 11.3,
                    yawSpeed: 0.4,
                    pitchSpeed: 0.35
                }
            }
            
            instances.plane = {
                project: jasmine.createSpy()
            }
            
            instances.actor = {
                collide: jasmine.createSpy()
            }
            
            instances.input = {
                leftStick: [0.2, -0.6],
                rightStick: [-0.3, 0.9]
            }
            
            player = {
                location: originalLocation = [3, 5, 7],
                previousLocation: [8, 4, 6],
                pitch: 0.12,
                previousPitch: 0.1,
                yaw: 15.6,
                previousYaw: 11.4
            }
        })
        
        it('calls actor.collide to handle collision', function(){
            part.tick(player)
            
            expect(instances.actor.collide.calls.count()).toEqual(1)
            expect(instances.actor.collide).toHaveBeenCalledWith(player)            
        })
        
        it('has copied location to previousLocation by this point', function(){
            instances.actor.collide.and.callFake(function(){
                expect(player.previousLocation).toEqual([3, 5, 7])
            })
            
            part.tick(player)
        })
        
        it('has applied the movement controls to location by this point', function(){
            instances.actor.collide.and.callFake(function(){
                // forward motion
                // cos 15.6 = -0.99417762518381522326367941424504
                // sin 15.6 = 0.10775365229944371013191427854299
                // 3 - (0.10775365229944371013191427854299 * 18.5 * 0.6) = 1.8039344594761748175357515081728
                // 5 + 0.99417762518381522326367941424504 * 18.5 * 0.6 = 16.03537163954034897822684149812
                
                // strafing
                // cos 15.6 = -0.99417762518381522326367941424504
                // sin 15.6 = 0.10775365229944371013191427854299
                // 1.8039344594761748175357515081728 + (-0.99417762518381522326367941424504 * 11.3 * 0.2) = -0.44290697343924758704016396802099
                // 16.03537163954034897822684149812 + (-0.10775365229944371013191427854299 * 11.3 * 0.2) = 15.791848385343606193328715228613           
                
                expect(player.location[0]).toBeCloseTo(-0.44290697343924758704016396802099)
                expect(player.location[1]).toBeCloseTo(15.791848385343606193328715228613)
                expect(player.location[2]).toBeCloseTo(7)
            })
            
            part.tick(player)
        })  
        
        it('then projects the collided location onto the surface of the navmesh tile and returns it', function(){
            instances.actor.collide.and.callFake(function(){
                player.tile = 'Test Output Tile'
                player.location[0] = 46.7
                player.location[1] = 52.7
                player.location[2] = 78.9
            })  
            
            instances.plane.project.and.callFake(function(plane, input, output){
                if(plane == 'Test Output Tile' && input == player.location && input == player.location) {
                    expect(instances.actor.collide).toHaveBeenCalled()
                    player.location[0] = 81.5
                    player.location[1] = 91.2
                    player.location[2] = 44.5
                }
            })              
            
            part.tick(player)
            
            expect(instances.plane.project).toHaveBeenCalledWith('Test Output Tile', player.location, player.location)
            
            expect(player.tile).toEqual('Test Output Tile')
            expect(player.location).toEqual([81.5, 91.2, 44.5])            
        })
        
        it('copies the yaw to previousYaw', function(){
            part.tick(player)
            
            expect(player.previousYaw).toEqual(15.6)
        })
        
        it('updates the yaw', function(){
            part.tick(player)
            
            expect(player.yaw).toEqual(15.48)
        })

        it('copies the pitch to previousPitch', function(){
            part.tick(player)
            
            expect(player.previousPitch).toEqual(0.12)
        })
        
        describe('when the pitch is between the upper and lower bounds', function(){
            it('updates the pitch', function(){
                part.tick(player)
                
                expect(player.pitch).toEqual(0.435)
            })
        })
        
        describe('when the pitch passes the upper bound', function(){
            it('updates the pitch, but stops at the upper bound', function(){
                instances.configuration.player.pitchSpeed = 2.0
                
                part.tick(player)
                
                expect(player.pitch).toEqual(0.45)
            })
        })    
        
        describe('when the pitch passes the lower bound', function(){
            it('updates the pitch, but stops at the lower bound', function(){
                instances.configuration.player.pitchSpeed = 2.0
                instances.input.rightStick[1] = -1.0
                
                part.tick(player)
                
                expect(player.pitch).toEqual(-0.36)                
            })
        })            
    })
})