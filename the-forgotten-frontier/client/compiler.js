/* Builds the GLSL fragment shader for a scene by putting together the glslLibraries
   specified in configuration found at glsl/[library].glsl and given scene GLSL,
   calling a given callback once completed. */
parts.compiler = function(instances) {
    return function(scene, callback){
        var index = 0
        var result = []
        var handler = function(data){
            if(data) result.push(data)
            if(index == instances.configuration.glslLibraries.length) {
                callback(result.join('\n'))
                return
            }
            var token = instances.configuration.glslLibraries[index++]
            if(!token) {
                result.push(scene)
                handler()
                return
            }
            
            instances.request('glsl/' + token + '.glsl', 'GET', handler)
        }
        handler()
    }
}