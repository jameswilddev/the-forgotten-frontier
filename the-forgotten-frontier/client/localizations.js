parts.localizations = function(){
    return {
        noWebGl: 'Could not get a WebGL context.  If you can, please make sure your browser is up to date, or try other browsers.',
        errorPreamble: 'The following error occurred:\n'
    }
}