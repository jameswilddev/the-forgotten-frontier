parts.configuration = function(){
    return {
        ticksPerSecond: 10,
        maximumDelta: 1.0,
        glslLibraries: ['misc', 'ray', 'camera', 'light', 'main', 'end'],
        player: {
            walkSpeed: 0.1,
            strafeSpeed: 0.075,
            pitchSpeed: 0.2,
            pitchUpper: 1.0,
            pitchLower: -0.6,
            yawSpeed: 0.2,
            eyeLevel: 1.6
        }
    }
}