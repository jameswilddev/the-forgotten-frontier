describe('analytics', function(){
    var part, instances
    beforeEach(function(){
        part = parts.analytics(instances = {})
    })
    
    describe('without first getting an analytics key', function(){
        it('does nothing on posting an error', function(){
            part.error()
        })
        
        it('does nothing on posting an update', function(){
            part.update()
        })        
    })
    
    describe('on calling', function(){
        var callback
        beforeEach(function(){
            callback = jasmine.createSpy()    
            instances.request = jasmine.createSpy()
            part(callback)
        })
        
        it('makes a request to get an analytics key', function(){
            expect(instances.request.calls.count()).toEqual(1)
            expect(instances.request).toHaveBeenCalledWith('analytics/start', 'POST', jasmine.any(Function))
        })
        
        it('does not execute the callback', function(){
            expect(callback).not.toHaveBeenCalled()
        })
        
        describe('on getting an analytics key', function(){
            beforeEach(function(){
                instances.request.calls.argsFor(0)[2]('"Test Key"')
            })
            
            it('executes the callback', function(){
                expect(callback).toHaveBeenCalled()
            })
            
            describe('on reporting an error', function(){
                beforeEach(function(){
                    part.error('Test Error')
                })
                
                it('sends it to the analytics service with the key', function(){
                    expect(instances.request.calls.count()).toEqual(2)
                    expect(instances.request).toHaveBeenCalledWith('analytics/error', 'POST', null, {
                        key: 'Test Key',
                        details: 'Test Error'
                    })
                })
                
                it('does not execute the callback again', function(){
                    expect(callback.calls.count()).toEqual(1)
                })
            })
            
            describe('on sending a player update', function(){
                beforeEach(function(){
                    part.update({
                        location: [4.5, 7.8, 3.4],
                        pitch: 0.6,
                        yaw: -4.3
                    }, 45)
                })                
                
                it('sends it to the analytics service with the key', function(){
                    expect(instances.request.calls.count()).toEqual(2)
                    expect(instances.request).toHaveBeenCalledWith('analytics/update', 'POST', null, {
                        key: 'Test Key',
                        x: 4.5,
                        y: 7.8,
                        z: 3.4,
                        pitch: 0.6,
                        yaw: -4.3,
                        framerate: 45
                    })
                })
                
                it('does not execute the callback again', function(){
                    expect(callback.calls.count()).toEqual(1)
                })                
            })
        })
    })
})