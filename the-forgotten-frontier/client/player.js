/* Extends actor to include pitch/yaw (in radians) and to take input for
   movement automatically. */
parts.player = function(instances){
    var part
    return part = {
        /* Call with a navmesh and a spawn point to return a new player there. */
        create: function(navmesh, spawn){
            var output = instances.actor.create(navmesh, spawn.location)
            output.pitch = output.previousPitch = spawn.pitch
            output.yaw = output.previousYaw = spawn.yaw
            return output
        },
        
        /* Call once per tick to update a given player's state. */
        tick: function(player) {
            instances.vector.clone(player.location, player.previousLocation)
            player.location[0] += Math.sin(player.yaw) * instances.input.leftStick[1] * instances.configuration.player.walkSpeed
            player.location[1] += Math.cos(player.yaw) * instances.input.leftStick[1] * instances.configuration.player.walkSpeed
            player.location[0] += Math.cos(player.yaw) * instances.input.leftStick[0] * instances.configuration.player.strafeSpeed
            player.location[1] -= Math.sin(player.yaw) * instances.input.leftStick[0] * instances.configuration.player.strafeSpeed
            player.previousPitch = player.pitch
            player.pitch = Math.min(instances.configuration.player.pitchUpper, 
                Math.max(instances.configuration.player.pitchLower, 
                    player.pitch + instances.input.rightStick[1] * instances.configuration.player.pitchSpeed
                )
            )
            player.previousYaw = player.yaw
            player.yaw += instances.configuration.player.yawSpeed * instances.input.rightStick[0]
            instances.actor.collide(player)
            instances.plane.project(player.tile, player.location, player.location)
        }
    }
}