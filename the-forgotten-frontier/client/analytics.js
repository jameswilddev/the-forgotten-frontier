// Handles communication with the analytics service.
parts.analytics = function(instances){
    var key
    // Call the part with a callback to start the analytics.
    var part = function(callback){
        instances.request('analytics/start', 'POST', function(keyData){
            key = JSON.parse(keyData)
            callback()
        })
    }
    
    // Call with details on an error to log those details if analytics have been started.
    part.error = function(details) {
        if(!key) return
        instances.request('analytics/error', 'POST', null, {
            key: key,
            details: details
        })
    }
    
    // Call with a reference to the player and the current framerate as an integer to log an update.
    part.update = function(player, framerate) {
        if(!key) return
        instances.request('analytics/update', 'POST', null, {
            key: key,
            x: player.location[0],
            y: player.location[1],
            z: player.location[2],
            pitch: player.pitch,
            yaw: player.yaw,
            framerate: framerate
        })
    }
    
    return part
}